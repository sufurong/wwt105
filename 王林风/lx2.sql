-- create table comments (
-- 	id INT primary key auto_increment comment'评论表 id',
-- 	News_id INT NOT NULL comment '新闻id',
-- 	comment1 VARCHAR(500) NOT NULL comment '评论',
-- 	Create_by VARCHAR(100) NOT NULL DEFAULT '创建时间' comment '创建人',
-- 	Create_date DATE NOT NULL comment '创建时间',
-- 	Update_by VARCHAR(100) NOT NULL DEFAULT '更改时间' comment '更新人',
-- 	Update_daye DATE NOT NULL comment '更新时间'
-- 	
-- );
-- alter table comments rename comments2;
-- alter table comments2 change column id id2 int;
-- alter table comments2 add (
-- 	update_time datetime,
-- 	is_del int default 0
-- );
-- 

create table a2 (
	id int primary key auto_increment	
	comment '新闻表id,主键(序列自动增长)',
	
	newstitle varchar(100) not null 
	default '新闻标题' 
	comment '新闻标题',
	
	content varchar(500) 
	comment '新闻内容',
	
	type int not null 
	default '0'
	comment '新闻类型,1社会,2娱乐、教育',
	
	statuss int not null
	default '0'
	comment '新闻状态,默认1有效,0无效',
	
	comment_num int 
	comment '评论数量,默认0',
	
	create_by varchar(100) not null
	default '创建人'
	comment '创建人',
	
	createdate datetime
	default '0-0-0'
	comment '创建时间',
	
	update_by varchar(100) not null
	default '更新人'
	comment '更新人',
	
	updatedate datetime
	default '0-0-0'
	comment '更新时间'
	
);

insert a2 values(null,'最新资讯','第一个作业',1,1,2333,'小王','2023-2-16 16:04:00','小王','0-0-0');

update a2 set newstitle='世界十大未解之谜' where statuss='1';

delete from a2 where newstitle like '%世界%';

select * from a2 where content like '%经典%'and newstitle not like'%世界%';

select * from a2 where statuss = 1 order by comment_num desc limit 10;

select * from a2 where newstitle like '________' and statuss = '1' and comment_num >= '1';

select * from a2 where createdate between '2023-1-1' and '2023-2-1';

select * from a2 where content is null and statuss = '0';

select * from a2 where create_by = update_by;

select statuss,count(*) from a2 group by statuss;

select type from a2 where createdate between '2022-7-1' and '2023-1-1' group by type having count(*)>10;

select type,avg(comment_num) from a2 group by type;