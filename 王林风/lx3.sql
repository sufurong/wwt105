-- 1.
SELECT c.name,a.SignIn FROM attendance AS a
LEFT JOIN class AS b 
ON a.classid = b.id
LEFT JOIN user AS c 
ON a.userid = c.id
WHERE a.signin = '0' and b.name = 'WWT101' and a.Date > '2023-2-15';

-- 2.
SELECT avg(a.sex) FROM user AS a
LEFT JOIN class AS b
ON a.classid = b.id;
ORDER BY sex;

-- 3
SELECT c.name,a.Stage,a.Score FROM score AS a
LEFT JOIN class AS b
ON a.classid = b.id
LEFT JOIN user AS c
ON a.userid = c.id
WHERE a.stage = '功能测试阶段' and a.score is null and b.name = 'WWT101';

-- 4
SELECT b.name,c.name,a.Date,a.SignIn FROM attendance AS a 
LEFT JOIN class AS b
ON a.classid = b.id
LEFT JOIN user AS c
ON a.userid = c.id
where a.Date = '2023-2-1'
ORDER BY a.ClassId
HAVING count(*) = (select number from class WHERE id = a.classid);

SELECT a.ClassId, COUNT(a.Id), c.`Name` FROM attendance a
INNER JOIN class c
on c.Id = a.ClassId
WHERE a.Date = '2023-02-01'
and a.SignIn = 1
GROUP BY a.ClassId
HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId);
