-- 1.查询emp表中全部员工信息
SELECT * FROM emp;
-- 2.查询emp表中的[员工姓名]、[工资]、[奖金]
SELECT ename ,sal,comm FROM emp;
-- 3.查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
SELECT empno as '员工编号' ,ename as '员工名称',hiredate as '入职时间' FROM emp;
-- 4.查询emp表中第3条开始，查询2条数据
SELECT * FROM emp LIMIT 2,2;
-- 5.查询emp表中的全部数据，根据[入职时间]降序排序
SELECT * FROM emp ORDER BY hiredate desc;
-- 6.查询emp表中工资最低的10个员工数据
SELECT * FROM emp ORDER BY sal ASC LIMIT 10;
-- 7.查询emp表中，直系上级领导编号不是“2”的员工信息
SELECT * FROM emp WHERE mgr != '2';
-- 8.查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
SELECT empno,ename,sal,hiredate FROM emp WHERE sal > 6000;
-- 9.查询emp表中，职位为'后端开发'的员工信息
SELECT * FROM emp WHERE job = '后端开发';
-- 10.查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
SELECT empno,ename,sal FROM emp WHERE sal >= 7000 and sal <= 10000;
-- 11.查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
SELECT * FROM emp WHERE job LIKE '%测试%' and sal<8000 ORDER BY empno desc;
-- 12.查询emp表中，工资大于6000，并且奖金不为空的员工信息
SELECT * FROM emp WHERE sal > 6000 and comm is not null;
-- 13.查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
SELECT * FROM emp WHERE job = '测试经理' or job = '产品经理';
-- 14.查询emp表中，员工编号是2、4、5的员工信息
SELECT * FROM emp WHERE empno in ('2','4','5');
-- 15.查询emp表中，职位不是'前端开发'的员工信息
select * from emp where job != '前端开发';
-- 16.查询emp表中奖金为null的员工信息
select * from emp where comm is null;
-- 17.查询emp表中有奖金的员工信息，且奖金不为0
select * from emp where comm is not null and comm>0;
-- 18.查询emp表中，员工姓名是‘黑’结尾的员工信息
select * from emp where ename like '%黑';
-- 19.查询emp表中，员工姓名包含‘红’字的员工信息
select * from emp where ename like '%红%';
-- 20.查询emp表中，员工姓名是两个字的员工信息
select * from emp where ename like '__';
-- 21.在emp表中，统计员工总人数
select count(empno) "总人数" from emp;
-- 22.在emp表中，统计员工平均工资
select floor(avg(sal)) '平均工资' from emp;
-- 23.在emp表中，统计入职最早的[员工姓名]
select ename from emp ORDER BY hiredate asc LIMIT 1;
-- 24.在emp表中，统计所有员工奖金总和
select sum(comm) "奖金总和" from emp;
-- 25.在emp表中，统计每个部门对应的员工人数
select deptno "部门",count(*) "人数" from emp GROUP BY deptno;
-- 26.在emp表中，统计每个部门的员工人数，以及该部门的平均工资
select deptno "部门",count(*) "人数",floor(avg(sal)) "平均工资" 
from emp 
GROUP BY deptno;
-- 27.在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
select ename,sal,deptno,max(sal) "该部门最高的工资" 
from emp 
where sal > 8000 
GROUP BY deptno;
-- 28.在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
select deptno "部门",count(deptno) "人数(倒序)" 
from emp 
GROUP BY deptno 
ORDER BY count(deptno) desc;
-- 29.查询emp表中，工资大于平均工资的员工信息
select * 
from emp 
where sal > (select avg(sal) from emp);
-- 30.查询emp表中，工资大于1号部门最高工资的员工信息
select * 
from emp 
where sal > (select max(sal) 
from emp 
where deptno = '1'
GROUP BY deptno);
-- 31.查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
select * 
from emp 
where deptno = (
select deptno 
from emp 
where ename = '小黑');
-- 32.查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
select * 
from emp 
where deptno != (
select deptno 
from emp 
where ename = '小黑');
-- 33.查询部门所在地区是'上海'的员工信息
select *
from emp as e
INNER JOIN dept as d
on e.deptno = d.deptno
where d.loc = '上海';
-- 34.查询emp表中，员工姓名以及姓名长度
select ename,CHAR_LENGTH(ename) from emp;
-- 35.查询emp表中，员工的姓名以及姓名的最后一个字符
select ename,right(ename,1) from emp;
-- 36.查询dept表中，查询[部门名称]和[地址]的字符拼接内容
select dname,loc,concat(dname,loc) from dept;
-- 37.查询emp表中，入职时间是去年的员工信息(现在是2023年2月18日)
select * from emp where hiredate<'2023-1-1';
-- 38.查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
select a.empno,a.ename,a.job,b.dname,b.loc
from emp as a
INNER JOIN dept as b
on a.deptno = b.deptno;
-- 39.查询所在地区在“上海”的员工有哪些，并根据工资升序排列
select *
from emp as e
INNER JOIN dept as d
on e.deptno = d.deptno
where d.loc = '上海'
ORDER BY sal;
-- 40.查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
SELECT e.ename '员工姓名', e.sal '工资', s.grade '工资等级' from emp e
left JOIN salgrade s
on e.sal between s.losal and s.upsal;
-- 41.查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
select a1.empno,a1.ename,a1.job,a1.mgr,(select ename from emp as a2 where a2.empno = a1.mgr) as '上级领导' from emp as a1;
