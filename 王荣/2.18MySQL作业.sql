-- 员工表

CREATE TABLE `emp`  (
  `EMPNO` int(11) NOT NULL COMMENT '员工编号',
  `ENAME` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `JOB` varchar(9) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位',
  `MGR` int(11) NULL DEFAULT NULL COMMENT '上级编号',
  `HIREDATE` date NULL DEFAULT NULL COMMENT '入职时间',
  `SAL` double NULL DEFAULT NULL COMMENT '工资',
  `COMM` double NULL DEFAULT NULL COMMENT '奖金',
  `DEPTNO` int(11) NULL DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`EMPNO`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

select*from emp;

INSERT INTO `emp` VALUES (1, '小白', '测试经理', NULL, '2019-11-22', 13000, 1000, 1);
INSERT INTO `emp` VALUES (2, '小黑', '测试组长', 1, '2022-09-06', 7000, NULL, 1);
INSERT INTO `emp` VALUES (3, '小蓝', '测试实习生', 2, '2022-11-02', 3000, NULL, 1);
INSERT INTO `emp` VALUES (4, '小红', '前端开发', NULL, '2022-08-30', 6000, 500, 2);
INSERT INTO `emp` VALUES (5, '小紫', '后端开发', NULL, '2021-08-11', 9000, 1000, 2);
INSERT INTO `emp` VALUES (6, '小绿', '产品经理', NULL, '2020-09-12', 17000, 2000, 3);

SET FOREIGN_KEY_CHECKS = 1;

-- 部门表
CREATE TABLE `dept`  (
  `DEPTNO` int(11) NOT NULL COMMENT '部门编号',
  `DNAME` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `LOC` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门地址',
  PRIMARY KEY (`DEPTNO`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

select*from dept;

INSERT INTO `dept` VALUES (1, '测试部', '上海');
INSERT INTO `dept` VALUES (2, '开发部', '北京');
INSERT INTO `dept` VALUES (3, '产品部', '杭州');
INSERT INTO `dept` VALUES (4, '客服部', '广州');

SET FOREIGN_KEY_CHECKS = 1;

-- 工资表
CREATE TABLE `salgrade`  (
  `GRADE` int(11) NULL DEFAULT NULL COMMENT '工资等级',
  `LOSAL` double NULL DEFAULT NULL COMMENT '最低工资',
  `abc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

select*from salgrade;

INSERT INTO `salgrade` VALUES (1, 0, '2000');
INSERT INTO `salgrade` VALUES (2, 2000, '5000');
INSERT INTO `salgrade` VALUES (3, 5000, '12000');
INSERT INTO `salgrade` VALUES (4, 12000, '20000');

SET FOREIGN_KEY_CHECKS = 1;

-- 1--
select*from emp;
-- 2--
select ENAME,SAL,COMM from emp;
-- 3--中文别名
select EMPNO as '员工编号',ENAME as '员工姓名',HIREDATE as '入职时间' from emp;
-- 4--
select*from emp limit 2,3;
-- 5--
select*from emp order by HIREDATE desc;
-- 6--
select*from emp order by SAL asc limit 10;
-- 7--
select EMPNO,ENAME,SAL,HIREDATE from emp where SAL>6000;
-- 8--
select*from emp where job like'后端开发'; 
-- 9--
select*from emp where MGR !=2;
-- 10--
select*from emp where SAL between 7000 and 10000;
-- 11--
select*from emp where  job like'%测试%' and SAL <=8000 order by EMPNO desc;
-- 12--
select*from emp where SAL>6000 and COMM is not null;
-- 13--
select*from emp where job in('测试经理','产品经理');
-- 14--
select*from emp where EMPNO in(2,4,5);
-- 15--
select*from emp where job not like '%前端开发%';
-- 16 --
select*from emp where comm is null;
-- 17--
select*from emp where comm is not null and comm !=0;
-- 18--
select*from emp where ENAME like '%黑';
-- 19--
select*from emp where ENAME like '%红%';
-- 20--
select*from emp where ENAME like '__';
-- 21--
select count(*) from emp EMPNO;
-- 22--
select avg(SAL) from emp;
-- 23--
select ENAME,min(HIREDATE) from emp;
-- 24--
select sum(comm) from emp;
-- 25--
select deptno,count(*) from emp GROUP BY deptno;
-- 26--
select deptno,count(*),avg(SAL) from emp GROUP BY deptno ;
-- 27--
select a.deptno,GROUP_CONCAT(a.ename,a.sal),
max(sal),b.dname
from emp a left join dept b
on a.deptno=b.deptno
where sal>8000 GROUP BY deptno;
-- 28--
select deptno from emp  group by deptno having count(*)>2 order by count(*) desc;
-- 29--
select*from emp where sal>(select avg(sal)from emp);
-- 30--
select*from emp where sal>(select max(sal)from emp where deptno=1);
-- 31--
select*from emp where deptno !=(select detno from emp where ename ='小黑') and ename !='小黑';
-- 32--
select*from emp where deptno !=(select detno from emp where ename ='小黑');
-- 33--
select e.* from emp e
inner join dept d
on e.deptno = d.deptno
where d.loc='上海';
-- 34--
select ename,CHAR_LENGTH(ename) from emp;
-- 35--
select ename,substr(ename,-1,1) from emp;
-- 36--
select conct(dname,loc) from dept;
-- 37--
select*from emp where year(HIREDATE)=year(now())-1;
-- 38--
select e.empno,e.ename,e.dname,d.loc from emp e
left join dept d
on e.deptno=d.DEPTNO;
-- 39--
select e.*from emo e
left join dept d
on e.deptno =d.DEPTNO
where d.loc='上海';
order by e.sal asc;
-- 40--
SELECT e.ename '员工姓名', e.sal '工资', s.grade '工资等级' from emp e
left JOIN salgrade s
on e.sal between s.losal and s.upsal;
-- 41--
select e1.empno, e1.ename, e1.job e2.empno, e2.ename
from emp e1,emp e2
where e1.mgr = e2.empno;