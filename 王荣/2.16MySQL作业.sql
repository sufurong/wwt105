drop table news;

create table news(
id int(10) primary key auto_increment comment'新闻表id' not null,
newstitle varchar(100) comment'新闻标题' not null,
content varchar(500) comment'新闻内容',
type int(20) comment'新闻类型' not null,
status int(10) comment'新闻状态,默认1有效,0为无效' not null,
comment_num int(10) comment'评论数量,默认0' ,
create_by varchar(100) comment'创建人' not null,
createdate datetime comment'创建时间' not null,
update_by varchar(100) comment'更新人' not null,
updatedate datetime comment'更新时间' not null
);
insert into news values(1,'新闻联播大结局','主播跑路',1,0,23,'wr','2023-02-16 16:02:30','rb','2023-02-16 16:04:50');
update news set status=1,newstitle='世界十大未解之谜';
insert into news values(2,'湖北省武汉市新闻','经典之家',1,1,1,'wr','2023-01-16 16:14:23','czh','2023-02-16 16:25:40');
insert into news values(3,'湖北省黄冈市新闻','经典之家',2,0,10,'dd','2022-09-16 16:14:23','wr','2023-02-16 16:25:40');
insert into news values(4,'湖北省黄冈市新闻',null,2,0,10,'wr','2023-09-16 16:14:23','wr','2023-02-16 16:25:40');
insert into news values(5,'湖北省黄冈市新闻',null,2,20,10,'wr','2023-09-16 16:14:23','wr','2023-02-16 16:25:40');
insert into news values(6,'湖北省黄冈市新闻',null,2,15,10,'wr','2023-09-16 16:14:23','wr','2023-02-16 16:25:40');
insert into news values(7,'湖北省黄冈市新闻',null,2,15,10,'wr','2023-09-16 16:14:23','wr','2023-02-16 16:25:40');
insert into news values(8,'湖北省黄冈市新闻','经典之家',2,0,10,'dd','2022-09-16 16:14:23','wr','2023-02-16 16:25:40');
insert into news values(9,'湖北省黄冈市新闻','经典之家',2,0,10,'dd','2022-09-16 16:14:23','wr','2023-02-16 16:25:40');

delete from news where newstitle like '%世界%';

select*from news where content like '%经典%' and newstitle not like '世界';

select*from news where status=1 order by comment_num desc limit 10;

select*from news where newstitle like'________' and comment_num>=1 and status=1;

select*from news where createdate between'2023-01-01'and'2023-01-30'and status=1;

select*from news where status=0 and content is null;

select c.create_by,c.update_by,c.* from news as c where c.create_by=c.update_by;
select*from nrws where create_by = update_by;

select status,count(*) from news group by status;

select type,count(type) from news 
where createdate between'2022-07-01' and '2022-12-30' 
GROUP BY type  
having count(*)>1;

select type,avg(comment_num) from news group by type;

select*from news;
