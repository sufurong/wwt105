-- 班级表
CREATE TABLE `class`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级编号',
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级名称',
  `Number` int(11) NOT NULL COMMENT '班级人数',
  `ClassHourCount` int(11) NOT NULL COMMENT '总课时',
  `CreateDate` datetime NOT NULL COMMENT '开班时间',
  `Grade` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后平均分等级：A/B/C/D/E',
  `IsEnd` int(11) NOT NULL COMMENT '是否结课：0未结课 1已结课',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;


-- 成绩表
CREATE TABLE `score`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '成绩单编号',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `UserId` int(11) NOT NULL COMMENT '学生编号',
  `Stage` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学习阶段',
  `Score` int(11) NULL DEFAULT NULL COMMENT '分数',
  `Remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;


-- 学生信息表
CREATE TABLE `user`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生编号',
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生名称',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `Sex` int(11) NULL DEFAULT NULL COMMENT '性别，0女 1男',
  `Age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `Grade` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后考试等级：A/B/C/D/E',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

insert into class values(1,'wwt105',5,10,'2022-02-15',null,0);
update class set id=1;
update class set grade='A' where id=1;

select*from class;


insert into attendance values(01,1,105,1,'2023-02-15',0);
insert into attendance values(02,2,105,3,'2022-02-1',1);
insert into attendance values(03,3,105,3,'2022-02-1',1);
insert into attendance values(04,4,105,3,'2022-02-1',1);
insert into attendance values(05,5,105,3,'2022-02-1',1);
insert into attendance values(06,1,105,1,'2023-02-1',1);
update attendance set ClassId=1;
delete from attendance where Date='2023-02-15';
update attendance set Date='2023-02-01';

select*from attendance;

insert into user values(001,'dd',1,0,18,null);
insert into user values(002,'cz',1,0,18,null);
insert into user values(003,'wr',1,1,22,null);
insert into user values(004,'lh',1,0,18,null);
insert into user values(005,'xh',1,1,22,null);

update user set grade='A' where id=1;
update user set grade='B' where id=2;
update user set grade='C' where id=3;
update user set grade='D' where id=4;
update user set grade='E' where id=5;
select * from user;

insert into score values(1,1,1,'功能测试阶段',null,null);
insert into score values(2,2,2,'功能测试阶段',null,null);
insert into score values(3,3,3,'功能开发阶段',100,null);
insert into score values(4,4,4,'功能测试阶段',null,null);
insert into score values(5,5,5,'功能测试阶段',null,null);
update score set score='100' where UserId=1;
update score set score='76' where UserId=2;
update score set score='65' where UserId=3;
update score set score='47' where UserId=4;
update score set score='10' where UserId=5;

select*from score;
-- 1--
select*from class a left join attendance b  
on a.id=b.ClassId left join user c 
on b.UserId=c.id 
where b.Date='2023-02-15'
and b.SignIn=0 
and a.Name='wwt105';
-- 2--
select*from user c 
left join class a 
on c.ClassId=a.id where count (c.Sex) 
from c.user div c.Sex=0;
-- 3--
select SUM(case when sex=1 then 1 else 0 end)/SUM(case when sex=0 then 1 else 0 end) 
from user c,class a 
where c.ClassId=a.Id and a.name='wwt105';
-- 4--
SELECT a.ClassId, COUNT(a.Id), c.`Name` FROM attendance a
INNER JOIN class c
on c.Id = a.ClassId
WHERE a.Date = '2023-02-01'
and a.SignIn = 1
GROUP BY a.ClassId
HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId);
-- 5--
SELECT '男平均' as '分组', avg(u.Age) as '年龄' FROM `user` u where u.Sex = 1
UNION
SELECT '女平均', avg(u.Age) FROM `user` u where u.Sex = 0
UNION
SELECT '校平均', avg(u.Age) FROM `user` u;
-- 6--
SELECT COUNT(U.Id), U.Grade FROM `user` U
LEFT JOIN class c
ON U.ClassId = C.Id
WHERE c.`Name` = 'WWT105'
GROUP BY U.Grade
-- 7--
SELECT avg(s.score) FROM `user` U
INNER JOIN class C
ON U.ClassId = C.Id
INNER JOIN score s
on s.UserId = u.Id
WHERE C.`Name` = 'WWT105'
and U.`Name` = 'dd';
-- 8--
DROP PROCEDURE if exists add_score; -- 删除存储函数

delimiter $$
CREATE PROCEDURE add_score(in className VARCHAR(50), in userName VARCHAR(50), in StageName VARCHAR(50), in addScore int)
BEGIN
DECLARE cid int;	-- 班级编号
DECLARE uid int;	-- 用户编号
set cid = (SELECT Id FROM class C WHERE C.`Name` = className);
set uid = (SELECT u.Id FROM class c, `user` u where c.Id = u.ClassId and C.`Name` = className AND u.`Name` = userName);
INSERT into score VALUES(DEFAULT, cid, uid, StageName, addScore, NULL);

UPDATE `user` SET GRADE = 
(SELECT case
	when Score<=100 and Score>=90 then 'A'
	when Score<=89 and Score>=75 then 'B'
	when Score<=74 and Score>=60 then 'C'
	when Score<=59 and Score>=45 then 'D'
	when Score<=44 and Score>=0 then 'E'
	ELSE
		'其他'
END '成绩等级' FROM score WHERE UserId = uid ORDER BY id desc limit 1 )
 WHERE id = uid;

UPDATE class set GRADE = 
(SELECT case
	when avg(Score) >=90 then 'A'
	when avg(Score) >=75 then 'B'
	when avg(Score) >=60 then 'C'
	when avg(Score) >=45 then 'D'
	when avg(Score) >=0 then 'E'
	ELSE '其他'
	END'平均分' FROM score WHERE ClassId = cid)
WHERE id = cid;

END $$
delimiter;
CALL add_score('WWT105', 'wr', '接口测试阶段', 99);