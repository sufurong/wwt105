-- 查看当前所有库
show DATABASES;
-- 切换到ceshi数据库
use ceshi;
--创建一个名为test01的数据库，字符编码集为utf8
create database test01 character set utf8;
-- 返回test01数据库的相关信息
show create database test01;
-- 删除数据库ceshi
drop database ceshi ;

use test01;

-- 创建一张数据表，名为user_info
create table user_info(
-- user_id列为int类型
user_id int(11),
-- user_name列为varchar类型，最大字符长度为30
user_name varchar(30),
-- user_age列为int类型，长度为3
user_age int(3)
);


-- 创建一张数据表，名为user_info02
create table user_info02(
-- uesr_id列为int类型，长度为11，并为单一主键
user_id int (11) primary key ,
-- user_namel列为varchar类型，最大字符长度为30
user_name varchar (30),
-- user_age列为int类型，长度为3
suer_age int (3)
);

-- 创建一张数据表，名为user_info03
create table user_info03(
-- user_id列为int类型，长度11
user_id int (11),
-- user_name列为varchar类型，最大字符长度为30
user_name varchar(30),
-- user_age列为int类型，长度为3
user_age int(3),
-- 将user_id列设置为单一主键
primary key (user_id)
);

-- 创建一张数据表，名为suer_info04
create table user_info04(
-- suer_id列为int类型，长度11
user_id int (11),
-- suer_name 列为varchar类型，最大字符长度为30
user_name varchar (30),
-- user_age列为int类型，长度为3
user_age int (3),
-- 创建表的同时，将user_id和user_name两列共同设置为一个联合主键
primary key (user_id,user_name)
);

-- 创建一张数据表，名为suer_info05
create table user_info05(
-- user_id列为int类型，长度为11，加唯一约束
user_id int (11) unique,
-- user_name列为varchar类型，最大字符长度为30
user_name varchar (30),
-- user_age列为int类型，长度为3
user_age int (3)
);

-- 新建一张数据表，名为user_info06
create table user_info06(
-- user_id列为int类型，长度为11
user_id int (11),
-- user_name 列为varchar类型，最大字符长度30
user_name varchar (30),
-- user——age 列为int类型，长度为3，设置默认值为18
user_age int (3) default '18'
);

-- 新建一张数据表，名为user_info07
create table user_info07(
-- user_id列为int类型，长度11
user_id int (11),
-- user_name列为varchar类型，最大字符长度为30， 且设置不能为空
user_name varchar (30) not null ,
-- user_age列类型为int ，长度3
user_age int (3)
);

-- 新建一个张数据表，名为user_info08
create table user_info08(
-- user_id l列为int类型，长度为11，设置为主键且该列自增
user_id int (11) primary key auto_increment,
-- 设置user_name列为varchar类型，最大字符长度30
user_name varchar (30),
-- user_age 列为int类型，长度为3
user_age int (3)
);

-- 新建一张数据表，名为user_info09
create table user_info09(
-- user_id 列为int类型，长度11，将user_id设置一个备注为“用户编号”
user_id int(11) comment '用户编号',
-- user_name 列为varchar类型，最大字符长度为30
user_name varchar (30),
-- user_age列为int类型，长度3
user_age int (3)
);


-- 新建一张数据表，名为user_info10
create table user_info10(
-- user_id列为int类型，长度为11
user_id int (11),
-- user_name 列为varchar类型，最大字符长度30
user_name varchar (30),
-- user_age列为int类型，长度3
user_age int (3)
-- 将该表的存储引擎设置为InnoDB，将字符编码设置为utf8
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 创建一张数据表，名为user_info11
create table user_info11(
-- user_id列为int类型，长度11，并设置为自增，且设置一个备注为“用户编号”
user_id int (11) auto_increment comment '用户编号',
-- user_name 列为varchar类型，最大字符长度为30，加上唯一约束，且设置一个备注“用户姓名”
user_name varchar (30) unique comment '用户姓名',
-- user_age列 为int类型，长度为3 ，且设置默认值为18，且不能为空，并设置一个备注为“用户年龄”
user_age int (3) default '18' not null comment '用户年龄',
-- 创建表的同时将user_id列设置为主键
primary key(user_id)
-- 将该表的存储引擎设置为innodb，将字符编码设置utf8
)engine=innodb default charset=utf8;

create table user_info12(
user_id int(11) primary key,
user_name varchar(30),
user_age int(3)
 );
 
--  查看全部表
show tables;

-- 查看表结构
desc user_info11;

-- 删除指定表
drop table user_info12;


-- 课后作业
create table comments(
id int not null primary key auto_increment comment '评论表id',
news_id int not null comment '新闻id',
comment varchar (500) not null comment '评论',
<<<<<<< HEAD
create_by varchar (100) not null DEFAULT 'id' comment '创建人',
create_date date not null comment '创建时间',
update_by varchar (100) not null DEFAULT 'id' comment '更新人',
=======
create_by varchar (100) not null comment '创建人',
create_date date not null comment '创建时间',
update_by varchar (100) not null comment '更新人',
>>>>>>> b9f4d95 (宁可累死自己，也要卷死大家)
update_date date not null comment '更新时间'
);

