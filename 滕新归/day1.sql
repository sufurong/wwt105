INSERT INTO text ( id, NAME, age )
VALUES
	( 1364721154, '小黑', 18 );
	
UPDATE text 
SET id = 0001 
WHERE
	id = 1364721154;
	
SELECT
	* 
FROM
	ceshi.text;
	
	-- 展示数据库
	show DATABASES;
	-- 指定数据库
	use ceshi;
	-- 创建数据库
	create DATABASE 库名字 CHARACTER SET 字符编码集（utf8）
	create DATABASE ceshi CHARACTER SET utf8;
	-- 查看指定库的信息
	-- CREATE DATABASE `ceshi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */
	show create database ceshi; 
	
	-- 删除库
	drop database 库名
	drop DATABASE ceshi;
	
	-- 创建表的基础语法
	create table user_01(
	id int(12) not null,
	name VARCHAR(20),
	age int(3),
	PRIMARY key (id)
);

	create table user_02(
	id int(12) not null PRIMARY key,
	name VARCHAR(20),
	age int(3)
);

-- 复合主键 PRIMARY key (键,键)
	create table user_03(
	id int(12) not null,
	name VARCHAR(20),
	age int(3),
	PRIMARY key (id,name)
);

-- 唯一约束 unique 被约束的建的值不可以唯一
	create table user_04(
	id int(12) not null unique,
	name VARCHAR(20),
	age int(3),
	PRIMARY key (id)
);

-- 设置默认的值 DEFAULT '值'
	create table user_05(
	id int(12) not null,
	name VARCHAR(20),
	age int(3) DEFAULT '18',
	PRIMARY key (id)
);

-- 非空约束 not null
-- 创建表，自带自增属性 auto_increment
	create table user_06(
	id int(12) not null auto_increment,
	name VARCHAR(20),
	age int(3),
	PRIMARY key (id)
);

-- 创建表时，备注属性 COMMENT ''
	create table user_07(
	id int(12) not null COMMENT '用户编号',
	name VARCHAR(20),
	age int(3),
	PRIMARY key (id)
);

-- 创建表带存储引擎和编码属性 ENGINE=INNODB DEFAULT CHARSET=utf8
	create table user_08(
	id int(12) not null,
	name VARCHAR(20),
	age int(3),
	PRIMARY key (id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 查看表 show tables
show tables;
-- 查看表的结构 desc user_01
desc user_01;
-- 删除指定表 drop table user_01
drop table user_01,user_02,user_03,user_04,user_05,user_06,user_07,user_08;


-- mysql 数据类型
-- Text类型： char（size），varchar（size），text，
-- number类型： int（size），double（size，d），
-- date类型：date(): yyyy-mm-dd  datetime(): yyyy-mm-dd hh-mm-ss 




create table comments(
id int(12) not null auto_increment COMMENT '评论表id',
news_id int(10) not null COMMENT '新闻id',
comment VARCHAR(500) not null COMMENT '评论',
create_by VARCHAR(100) not null DEFAULT 'id' COMMENT '创建人',
create_date date not null COMMENT '创建时间',
update_by VARCHAR(100) not null DEFAULT 'id' COMMENT '更新人',
update_date date not null COMMENT '更新时间',
PRIMARY key(id)
);















	
	