use ceshi;

-- 子查询 在一个select语句中嵌入了另外一个语句，那么被嵌入的select语句被称为子查询语句 外部的查询称为主查询 

-- 查询大于平均年龄的学生
select * from students where age>(select avg(age) from students);

-- 联合查询 内联查询 INNER JOIN 或 JOIN 左联查询 LEFT JOIN 右联查询 RIGHT JOIN 

-- 内联查询：select * from 表A inner JOIN 表B on 表A.列=表B.列
select * from emp e INNER JOIN dept d on e.deptno=d.deptno where e.sal>=5000 ORDER BY sal desc ;

-- 左联查询 左表为主表 右表为辅助表：select * from 表A left JOIN 表B on 表A.列=表B.列
select * from emp e LEFT JOIN dept d on e.deptno=d.deptno WHERE d.loc='上海';

-- 右联查询 右表为主表 左表为辅助表：select * from 表A right JOIN 表B on 表A.列=表B.列
select * from emp e RIGHT JOIN dept d on e.deptno=d.deptno where d.dname='测试部';

-- 自联查询 select * from 表A as A inner JOIN 表B as B on A.列=B.列
select * from emp e INNER JOIN emp e2 on e.mgr=e2.empno;


-- 字符串函数 ASCII(str)  返回ASC值
select ASCII('A');

-- CHAR_LENGTH(str) 计算字符串的长度
select CHAR_LENGTH(newstitle) 新闻字数 from news;
select CHAR_LENGTH('哈利波特');

-- CONCAT(str1,str2,...) 拼接字符串
select CONCAT('你好','daw');
select CONCAT(sal,job,loc) from emp,dept where emp.DEPTNO=dept.DEPTNO;

-- FORMAT(number,decimal_places) 函数将数字格式化为像"#,###,###.##"这样的格式，四舍五入到指定的小数位数，然后将结果作为字符串返回  
-- number:必填，你需要格式化的数字  decimal_places：必填，0代表没有小数位，1代表只有一个小数位，以此类推，采用四舍五入
SELECT FORMAT(250500.5634, 2);

select format(sal,0) from emp ORDER BY emp.sal desc;


-- LOWER(str) 将字符串中的大写转为小写
select LOWER('AWDAWDAEF');
select LOWER(ename) from emp;

-- upper() 将小写转为大写
select UPPER('dawdawd');
select UPPER(ename) from emp;

-- SUBSTR(s,start,length) 截取字符串
select substr('为世界经济阿达',2,4); -- 从第二个字开始截取四个字符
select substr(job,1,2) from emp;

-- trim(s) 去掉前后空格
select trim(' 11 22   ');
select LTRIM('   11 ');
select RTRIM('  11      ');
select trim(ename) from emp;


-- rand() 随机数
select RAND();-- 0~1之间的值
select RAND()*10; -- 1~10之间的值
select FORMAT(RAND()*100,0); -- 1~100之间的值 and 整数

-- 日期函数
-- ADDDATE(日期,加的天数) 
select ADDDATE('2023-1-10',10);
select ADDDATE(createdate,10) from news where 

-- ADDTIME(日期时间,加的时间)
select ADDTIME('2023-1-1 12:22:23','1:12:25');

-- DATEDIFF(开始日期,结束日期) 计算时间差
select DATEDIFF('2023-01-010','2023-01-03');

-- DATE_ADD(d,type) 计算起始日期d加上一个时间段之后的日期，type值有很多
select DATE_ADD('2023-1-12',INTERVAL 10 DAY);
-- DATE_SUB(d,type) 计算起始日期d减去上一个时间段之后的日期，type值有很多
select DATE_SUB('2023-1-12',INTERVAL 10 DAY);

-- DATE_FORMAT(日期表达式,日期格式)
select DATE_FORMAT('20230220','%y-%m-%d');


-- YEAR(日期时间) 截取年份
select YEAR(createdate) from news;
-- MONTH() 
select MONTH(createdate) from news;
-- DAY(date)
select DAY(createdate) from news;
-- time() 
select TIME(createdate) from news;


-- curdate() 返回系统日期
select CURDATE()
select * from news where createdate>=CURDATE(); -- 近七天发布的新闻

-- curtime()
select CURTIME()


-- case表示函数开始 end表示函数结束select case sex
WHEN 1 THEN '男'
WHEN 0 THEN '女'
else '未知'
end sex,stu_name as 姓名
from students;


select case deptno
when 1 THEN '测试'
when 2 THEN '开发'
when 3 THEN '产品'
when 4 THEN '客服'
else '未知部门'
end 部门,loc as 城市
from dept;

-- if 函数
select if(age>=18,'成年','未成年') from students;

SELECT if(mgr is null ,'没有上级',mgr) from emp;

-- IFNULL(expr1,expr2) 函数 age如果不是null 返回实际值 如果是null 返回expr2
select IFNULL(age,'未填写年龄') from students;

-- ISNULL(expr) 是null 0  不是null 1
select ISNULL(age) from students;


-- NULLIF(expr1,expr2) 如果expr1等于expr2 返回数据 否则返回null
select NULLIF(create_by,update_by) from news;


-- select database(); 返回当前所在数据库


1.查询班级名称为WWT105的班级,在2023年2月15日未签到的学生名单
select name from User INNER JOIN attendance on user.id=attendance.Userid where User.classid=(select id from class where name='ttw105') and Date BETWEEN'2023-2-15' AND '2023-2-16' and Signin=0;

select * from attendance att 
LEFT JOIN user u1 on att.userid=u1.id 
LEFT JOIN class cl on att.ClassId=cl.id 
where att.Date BETWEEN'2023-2-15' AND '2023-2-16' and Signin=0;

2.查询班级名称为WWT105的班级的男女比例
select sum(case when sex=1 THEN 1 else 0 end)/
			 sum(case when sex=0 then 1 else 0 end)
from user where classid=(select id from class where name='ttw105');

3.查询班级名称为WWT105的班级，在学习阶段为“功能测试阶段”，未参加考试的名单(即，分数=null)
select * from score where classid=(select id from class where name='ttw105') 
and stage='功能测试' and score is null;

select * from score sc 
LEFT JOIN class cl on sc.classid=cl.id 
LEFT JOIN user u1 on sc.userid=u1.id
where cl.name='ttw105' 
and sc.Stage='功能测试' 
and sc.Score is null;

4.查询2023年2月1日,全员都签到的班级名称
select count(name) from class, attendance att where class.id=att.classid and att.Signin=1 and att.date between '2023-1-31' and '2023-2-2' GROUP BY class.id;

select name from class where number in(select count(name) from class, attendance att where class.id=att.classid and att.Signin=1 and att.date between '2023-1-31' and '2023-2-2' GROUP BY class.id);


SELECT a.ClassId, COUNT(a.Id), c.`Name` FROM attendance a
INNER JOIN class c
on c.Id = a.ClassId
WHERE a.Date between '2023-1-31' and '2023-2-2' 
and a.SignIn = 1
GROUP BY a.ClassId
HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId);


5.查询全校的男生平均年龄、女生平均年龄、全校平均年龄
SELECT avg(age) as 男生平均年龄,(SELECT avg(age) as 女生平均年龄 from user where sex=0),(select avg(age) as 全校平均年龄 from user) from user where sex=1

SELECT '男平均' as '分组' , avg(age) as 平均年龄 from user where sex=1
UNION
select '女平均', avg(age) as 女生平均年龄 from user where sex=0
UNION
select '校平均' , avg(age) as 全校平均年龄 from user

SELECT '男平均' as '分组', avg(u.Age) as '年龄' FROM `user` u where u.Sex = 1
UNION
SELECT '女平均', avg(u.Age) FROM `user` u where u.Sex = 0
UNION
SELECT '校平均', avg(u.Age) FROM `user` u;

6.查询班级名称为WWT105的班级,成绩ABCDE每个等级的人数分别有多少
select grade,GROUP_CONCAT(name) from user where classid=(select id from class where name='ttw105') GROUP BY grade;


7.查询班级名称WWT105班级里，名为“阿龙”的同学参加考试的所有成绩单平均分
select avg(sc.score) from score sc 
INNER JOIN class cl on sc.classid=cl.id
INNER JOIN user u1 on u1.id=sc.userid
where cl.name='ttw105'
and u1.name='龙哥'


SELECT avg(s.score) FROM `user` U
INNER JOIN class C
ON U.ClassId = C.Id
INNER JOIN score s
on s.UserId = u.Id
WHERE C.`Name` = 'ttw105'
and U.`Name` = '龙哥';


8.假设分数100~90=A级、89~75=B级、74~60=C级、59~45=D级、44~0=E级，输录入WWT105的“阿龙”同学成绩单，并更新相关表(需更新score成绩表、User学生用户表、Class班级表)

