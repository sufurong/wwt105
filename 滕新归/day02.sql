use ceshi;

-- 修改表名 alter table 旧表名 rename 新表名
alter table comments rename commentsnew;
alter table commentsnew rename comments;

-- 修改字段和数据类型 alter table 表名 change column 旧字段名 新字段名 数据类型
alter table comments CHANGE column id id_02 int;
alter table comments change column id_02 id int;

-- 仅修改字段数据类型 alter table 表名 modify 字段名 数据类型;
alter table comments MODIFY id double;
alter table comments MODIFY id int;

-- 删除字段 alter table 表名 drop column 字段名
alter table comments drop column update_date;

-- 新增字段 alter table 表名 add(新增字段1 数据类型，新增字段2 数据类型，);
alter table comments add(
update_date date
);


alter table comments rename students;

alter table students add(
stu_name varchar(30),
age int(3),
sex int(2),
province VARCHAR(20),
city VARCHAR(50)
);

alter table students drop column update_date;


-- 四大约束 PRIMARY key 主键,FOREIGN key 外键,UNIQUE 唯一,not null 非空

show keys from students

-- 违反主键约束
-- 1062 - Duplicate entry '1' for key 'students.PRIMARY'
INSERT into students(id,stu_name,age,sex,province,city)VALUES(0001,'小黑',12,0,'111','背景'),
(0001,'小黑',12,0,'111','背景');

-- 组合主键 两个字段及以上组合成主键

-- 插入语句 insert into 表名 values(列值1，列值2.。。。)
-- (数据，数据。。。)
insert into students(id,stu_name,age,sex,province,city)values(0001,'小黑',18,1,'1','东北'),
(0002,'小红',36,0,'2','北京'),
(0003,'小黄',20,1,'3','上海'),
(0004,'小蓝',25,1,'4','湖北');

insert students(id,stu_name,age)values(0006,'小绿',55),(0005,'QQ',12);


-- 更新某一列的值 update 表名 set 列名=新值， 列名2=新值

UPDATE students set city='呼伦贝尔';

-- 筛选数据之后，再更新某一列的值 update 表名 set 列值=新值 where 条件

update students set city='上海' where id=0001;
update students set city='北京' where stu_name='小红';

-- delete 删除语句 delete from 表名
delete from students;--删除表的所有数据
delete from students where id=6; -- 删除id=6这一行数据


-- 查询全部列数据 select * from 表名
select * from students;

-- 查询指定列数据 select 列名1，列名2.。。。 from 表名
select id ,stu_name from students;

-- 查询时 使用别名代替表名 select 表别名.字段名 from 表名 as 别名
select m.stu_name from students as m;
select st.stu_name as 姓名 from  students st ;

-- 查询使用别名代替字段名 as可以省略
select stu_name as 姓名,sex as 性别 from students;


-- 限制查询（分页查询）SELECT * from 表名 limit 初始位置，行数
select * from students limit 2,3;

select  * from students limit 3;-- 查询前3行数据


-- 排序查询 select * from 表名 ORDER BY 列名 排序规则（ASC DESC）
select * from students ORDER BY id desc;-- id倒序
select * from students ORDER BY age asc; -- age升序排列

select * from students ORDER BY age asc,id desc;

-- 查询年龄最大，显示10行数据
select stu_name,age from students ORDER BY age desc LIMIT 10;

-- 去重查询 distinct
select distinct stu_name from students;

-- where 查询 比较运算符 = > < <= >= !=或<>。
-- 						逻辑运算符 and or not 
-- 						

select * from students WHERE id>=3;
select * from students where id<>3;

select * from students where sex=1 and age>=20;
select * from students where city is not null and age>18;
select * from students where not stu_name='小黑';
select * from students where age<=25 or sex=0;


-- like 模糊查询 _表示匹配1个字符 %表示匹配任意个字符
select * from  students where stu_name like '小%';
select * from  students where stu_name like '小_';
select * from  students where stu_name like '%黄';
select * from  students where stu_name like '__';


-- BETWEEN 范围查询 BETWEEN AND
select * from students where age BETWEEN 20 and 25;

-- 空判断 空：is null  非空：is not null
select * from students where city is null;
select * from students where city is not null;

-- 枚举 in 在指定的几个数据中选值
select * from students where stu_name in('QQ','小黑');


-- 分组查询 GROUP BY 将查询结果按照指定字段进行分组，字段中数据相等的分为一组

select * from students GROUP BY city;
select count(sex) as 男女 from students GROUP BY sex;
select province,city from students GROUP BY province,city;

-- GROUP_CONCAT(expr):统计
select province,GROUP_CONCAT(stu_name) from students GROUP BY province;

INSERT students(id,stu_name,province)values(6,'oo',1);

-- GROUP BY +聚合函数 count()  sum()  avg()  min()  max()
select province,count(*) from students GROUP BY province;

select sum(age) from students;
select avg(age) from students;

-- GROUP BY + having 
select * from students GROUP BY age having age>20;

select province students from students where sex=0 GROUP BY province HAVING count(*)>10;

-- 书写顺序
select *
from 表名
where 条件
group by 列名
having 条件
order by 列名/聚合函数 排序规则
limit 0,10;




create table news(
id int not null auto_increment COMMENT '新闻id',newstitle VARCHAR(100) not null COMMENT '新闻标题',
content VARCHAR(500) COMMENT '新闻内容',
type int not null COMMENT '新闻类型 1社会 2娱乐 3教育',
news_status int not null DEFAULT '1' COMMENT '新闻状态 默认1有效 0无效',
comment_num int DEFAULT '0',-- 评论数量
create_by VARCHAR(100) not null COMMENT '创始人',
createdate datetime not null COMMENT '创始时间',
update_by VARCHAR(100) not null COMMENT '更新人',
updatedate datetime not null COMMENT '更新时间',
PRIMARY key(id)
);

1编写1条插入语句
insert into news(id,newstitle,content,type,news_status,comment_num,create_by,createdate,update_by,updatedate)VALUES(007,'世界十大未解之谜','蜘蛛侠打击罪犯经典',1,1,10001,'皮特','2000-10-12 12-30-56','鲍勃','2000-10-12 13-30-00');

insert into news(id,newstitle,content,type,news_status,comment_num,create_by,createdate,update_by,updatedate)VALUES(003,'GG爆重生','打败坏蛋',1,0,10001,'皮特','2000-10-12 12-30-56','皮特','2000-10-12 13-30-00'),
(004,'美国核泄漏','死伤超100w人',1,0,10001,'皮特','2000-10-12 12-30-56','皮特','2000-10-12 13-30-00');

insert into news(id,newstitle,content,type,news_status,comment_num,create_by,createdate,update_by,updatedate)VALUES(006,'日本发现美人鱼','玩玩玩了',2,1,20001,'皮特','2023-1-12 12-30-56','鲍勃','2000-10-12 13-30-00');

2．编写1条修改语句，将[新闻状态]为1的数据，[新闻标题]更新为“世界十大未解之谜”
update news set newstitle='世界十大未解之谜' where news_status=1;


3．编写1条删除语句，将[新闻标题]包含“世界”的数据删除
DELETE from news where newstitle like '%世界%';

4．查询[新闻内容]包含“经典”，[新闻标题]不包含“世界”的数据
select  * from news where content like '%经典%' and newstitle not LIKE '%世界%';
select  * from news where content like '%经典%' and not newstitle LIKE '%世界%';


5．查询[评论数量]最多的10条的有效新闻
select * from news where news_status=1 ORDER BY comment_num desc LIMIT 10;

6.查询[新闻标题]是8个字的，且至少已有一条评论的有效新闻
select * from news where newstitle like '________' and comment_num>=1 and news_status=1;

7．查询[创建时间]是2023年1月份的有效新闻
select * from news where createdate < '2023-2-1 00-00-00' and createdate> '2022-12-31 23-59-59' and news_status=1;

select * from news where createdate BETWEEN '2022-12-31 23-59-59' and '2023-2-1 00-00-00' and news_status=1;

8．查询未录入[新闻内容]的无效数据
select * from news where content is null and news_status=0;

9．查询[创建人]和[更新人]是同一人的新闻
select * from news where create_by=update_by;

10.查询统计有效新闻和无效新闻的数量
SELECT count(*) as 有效新闻和无效新闻的数量 from news GROUP BY news_status;

11.查询在2022年下半年发布的新闻中，数量大于10条的[新闻类型]有哪些
select GROUP_CONCAT(type) from news where  createdate between '2022-5-31 23-59-59' and '2023-1-1 00-00-00' GROUP BY type HAVING count(*)>10;


12.计算每种[新闻类型]对应的平均评论数量
select type,avg(comment_num) from news GROUP BY type;


