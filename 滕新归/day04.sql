use ceshi;

1查询emp表中全部员工信息
select * from emp;

2查询emp表中的[员工姓名]、[工资]、[奖金]
select ename 员工姓名,sal 工资,comm 奖金 from emp;

3查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
select empno 员工编号,ename 员工姓名, hiredate 入职时间 from emp;

4查询emp表中第3条开始，查询2条数据
select * from emp LIMIT 2,2;

5查询emp表中的全部数据，根据[入职时间]降序排序
select * from emp ORDER BY hiredate desc;

6查询emp表中工资最低的10个员工数据
SELECT * from emp ORDER BY sal LIMIT 0,10;

7查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
select empno 员工编号,ename 姓名,sal 工资,hiredate 入职时间 from emp where sal>6000;

8查询emp表中，职位为'后端开发'的员工信息
select * from emp WHERE job='后端开发';

9查询emp表中，直系上级领导编号不是“2”的员工信息
select * from emp where mgr <> 2;

10查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
select empno 员工编号,ename 姓名,sal 工资 from emp where sal between 7000 and 10000;

11查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
select * from emp where job like '%测试%' and sal <= 8000 ORDER BY empno desc;

12查询emp表中，工资大于6000，并且奖金不为空的员工信息 
select * from emp where sal >6000 and comm is not null;

13查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
select * from emp where job='测试经理' or job='产品经理';

14查询emp表中，员工编号是2、4、5的员工信息
select * from emp where empno in(2,4,5);

15查询emp表中，职位不是'前端开发'的员工信息
select * from emp where job <> '前端开发';

16查询emp表中奖金为null的员工信息
select * from emp where comm is null;

17查询emp表中有奖金的员工信息，且奖金不为0
select * from emp where comm is not null;

18查询emp表中，员工姓名是‘黑’结尾的员工信息
select * from emp where ename like '%黑';

19查询emp表中，员工姓名包含‘红’字的员工信息
select * from emp where ename like '%红%';

20查询emp表中，员工姓名是两个字的员工信息
select * from emp where ename like '__';

21在emp表中，统计员工总人数
select count(empno) 员工总人数 from emp;

22在emp表中，统计员工平均工资
select avg(sal) 员工平均工资 from emp;

23在emp表中，统计入职最早的[员工姓名]
select * from emp ORDER BY hiredate asc LIMIT 1;

24在emp表中，统计所有员工奖金总和
select sum(comm) 员工奖金总和 from emp;

25在emp表中，统计每个部门对应的员工人数
select dept.DNAME 部门名称,count(empno) 员工人数 from emp INNER JOIN dept on emp.deptno=dept.DEPTNO GROUP BY emp.deptno;

26在emp表中，统计每个部门的员工人数，以及该部门的平均工资
select dept.DNAME 部门名称,count(empno) 员工人数,avg(emp.sal) 平均工资 
from emp 
INNER JOIN dept 
on emp.deptno=dept.DEPTNO 
GROUP BY emp.deptno;

27在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
select * from emp 
left JOIN dept 
on emp.DEPTNO=dept.DEPTNO 
where emp.sal>8000
ORDER BY emp.sal desc;


28在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
select emp.deptno,dept.DNAME from emp 
INNER JOIN dept
on emp.DEPTNO=dept.DEPTNO
GROUP BY emp.deptno
HAVING count(*)>2
ORDER BY emp.deptno desc;

select deptno from emp GROUP BY deptno HAVING count(*)>2 ORDER BY deptno desc

29查询emp表中，工资大于平均工资的员工信息

select * from emp where sal>(select avg(sal) from emp);

30查询emp表中，工资大于1号部门最高工资的员工信息



select * from emp where sal>(select max(sal) from emp where deptno=1)

31查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
select * from 
(select * from emp where ename<>'小黑') as a where a.deptno=(SELECT deptno from emp where ename='小黑');

32查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
select * from emp where deptno<>(select deptno from emp where ename='小黑');

33查询部门所在地区是'上海'的员工信息
select * from emp 
INNER JOIN dept
on emp.DEPTNO=dept.DEPTNO
where dept.LOC='上海';

34查询emp表中，员工姓名以及姓名长度
select ename,CHAR_LENGTH(ename) 姓名长度 from emp;

35查询emp表中，员工的姓名以及姓名的最后一个字符
select ename,right(ename,1) from emp ;
select ename,SUBSTR(ename,-1,1) from emp;

36查询dept表中，查询[部门名称]和[地址]的字符拼接内容
select CONCAT(dname,loc) from dept;

37查询emp表中，入职时间是去年的员工信息
select YEAR(now()) -1;
select TIME(now());


-- 老师的答案
SELECT * from emp where year(HIREDATE) = YEAR(now()) -1

select * from emp where HIREDATE 
between YEAR(now()) -2 
and YEAR(now()) -1 ;


38查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
select e.empno 员工编号,e.ename 员工姓名,e.job 职位,d.DNAME 部门名称,d.loc 部门地址
from emp e
INNER JOIN dept d
on e.DEPTNO=d.DEPTNO


39查询所在地区在“上海”的员工有哪些，并根据工资升序排列
select * from emp e
INNER JOIN dept d
on e.DEPTNO=d.DEPTNO
where d.loc='上海'
ORDER BY e.sal;

40查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
select GROUP_CONCAT(e.ename),GROUP_CONCAT(e.sal),s.grade 
from emp e 
right JOIN salgrade s 
on e.sal between s.losal and s.upsal
GROUP BY s.grade
ORDER BY s.grade desc;

SELECT e.ename '员工姓名', e.sal '工资', s.grade '工资等级' from emp e
left JOIN salgrade s
on e.sal between s.losal and s.upsal;

41查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]

select e.empno 员工编号,e.ename 员工姓名,e.job 职位,e2.empno 上级领导编号,e2.ename 上级领导姓名 
from emp e
INNER JOIN emp e2
on e.mgr=e2.empno




1统计昨日收益
select curdate() -1;

select sum(day_money_amount) 昨日收益
from profit_statistics
where date(create_date) = curdate() -1;

select sum(ps.day_money_amount)
from profit_statistics ps
where ps.user_id=1
and is_del=0;
and date_fromat(ps.statistics_time,'%y-%m-%d') = CURRENT_DATE();


2统计累计收益
select sum(day_money_amount) 累计收益
from profit_statistics;


3统计近7日折线图节点



4统计单篇作品收益信息
select * from task_detail GROUP BY work_id;


select CONCAT_WS('1','2','3');
select CONCAT('1','2','3');
select FIELD('c','a','c','b');
SELECT FIND_IN_SET('o','h,e,l,l,o');