

create table student(
id int primary key,
name char(11) unique not null,
age int not null,
sex int not null,
city char(10) not null );

insert into values(2,'张三',18,0,'武汉');





insert student(id,name,age,sex,city)values(1,'张三',18,0,'武汉');
insert student(id,name,age,sex,city)values(2,'李四',19,0,'武汉');
insert student(id,name,age,sex,city)values(3,'王五',20,0,'武汉');
insert student(id,name,age,sex,city)values(4,'小白',21,1,'武汉');

update student set name='韩韩' where id=1;

select*from student;
select name,age from student;

select u.name from student as u;
select name as'姓名' from student;
select name 姓名,age from student;
select name,age from student limit 0,2

select*from student order by age desc;
select*from student order by age desc limit 0,2;
select distinct city from student;
select*from student where id > 3;
select*from student where name !='小白';
select*from student where id <=4
select*from student where age=18;
select*from student where name like '韩%';
select*from student where name like '_四';
select*from student where age between 18 and 20;
alter table student change  column userage age int;
select name from student group by city;
select*from student where name in ('张三','李四');
select group_concat(name) from student group by city;
select city,count(*) from student GROUP BY city;
select group_concat(name),sum(age) from student group by city;
update student set city='襄阳' where id=3;
select city from student group by city having count(*)>2;





create table news(
id int not null comment '新闻表id' primary key,
newstitle varchar(100) not null comment'新闻标题',
content varchar(500) comment'新闻内容',
type int not null comment'新闻类型，1社会、2娱乐、教育',
status int not null comment'新闻状态，默认1有效，0为无效',
comment_num int comment'评论数量，默认为0',
create_by varchar(100) not null comment'创建人',
createdate datetime not null comment'创建时间',
update_by varchar(100) not null comment'更新人',
updatedate datetime not null comment'更新时间')

insert into news (id,newstitle,type,status,create_by,createdate,update_by,updatedate)values(7,'老逼拜登',1,1,'张三','2001-6-16','拜登','2001-6-19');
insert into news values(1,'老逼拜登','拜登死了',1,1,20,'张三','2001-6-16','拜登','2001-6-19');
insert into news values(2,'横说竖说','时间是是是',2,0,5,'李四','2001-6-16','拜登','2001-6-19');
insert into news values(3,'水水水水是是是是','事实经典实上',1,1,20,'王五','2001-6-16','拜登','2001-6-19');
insert into news values(4,'你好世界','不好经典',1,0,5,'赵六','2001-6-16','拜登','2001-6-19');
insert into news values(5,'是的是的所多所多所多所多所多所','上树了',1,1,20,'试试','2001-6-16','拜登','2001-6-19');
insert into news values(6,'一月又一月','颠三倒四多所多所',2,1,20,'拜登','2023-1-16','拜登','2001-6-19');


update news set newstitle='世界十大未解之谜' where status=1;
delete from news where newstitle like '%世界%';
select *from news where content like'%经典%' and newstitle not like '%世界%';
select  *from news where status=1 order by comment_num desc;
select *from news where newstitle like '________' and status=1 having comment_num>=1;
select *from news where status=1 and createdate between ('2023-1-1')and('2023-1-31');
select *from news where content is null;
select *from news where create_by = update_by;
select status,count(*) from news group by status;
select  type from news where createdate between ('2023-1-1') and ('2023-6-30');
select type,avg(comment_num) from news group by type;
SELECT type
FROM news
WHERE createdate BETWEEN '2022-07-01' and '2022-12-31'
GROUP BY type
HAVING count(*) > 10;



select* from student as s,class as c where class_id in (select id from class where id ==1)
select * from student inner join class on ('条件');



select *from emp left join dept on emp.deptno=dept.deptno;
select *from emp e inner join emp p on e.empno=p.mgr;
select char_length (ename) from emp;
select concat(ename,job)from emp;
select format('29999.323232',3);
select lower('ABC');
select upper('abc');
select substr('老逼拜登',2,1);
select trim(' 淫 恒瑞 ');
select rand(); --+-*/
select floor(rand());
select adddate('2023-01-20',interval 10 day);
select addtime('2023-01-20 1:21:09','2:33:19');
select datediff('2023-02-17','2022-04-28');
select date_sub('2021-9-14',interval 10 day);
select date_add('2021-9-14',interval 10 day);
select date_format('2021914','%Y-%M-%D');
select year('2023-10-12 10:29:23');
select curtime();
select case sex when 1 then '男'
when 2 then '女'
else '未知'
end case;












