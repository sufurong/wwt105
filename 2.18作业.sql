Select * from emp;
select ename,sal,comm from emp;
select empno as '员工编码',ename as '员工姓名',hiredate as '入职时间' from emp;
select *from emp limit 2,2;
select * from emp ORDER BY hiredate desc;
select * from emp ORDER BY sal asc limit 0,10;
select empno,ename,sal,hiredate from emp where sal > 6000;
select * from emp where job = '后端开发';
select * from emp where mgr != 2 or mgr is null;
select empno,ename,sal from emp where sal >7000 having sal <10000;
select * from emp where job like '%测试%' having sal <=8000 ORDER BY empno desc;
select * from emp where sal >6000 having comm is not null;
select * from emp where job = '测试经理' or job = '产品经理';
select * from emp where empno = 2 or empno = 4 or empno =5;
select * from emp where not job = '前端开发';
select * from emp where comm is null;
select * from emp where comm is not null;
select * from emp where ename like '%黑';
select * from emp where ename like '%红%';
select * from emp where ename like '__';
select COUNT(ename) from emp;
select FORMAT(AVG(sal),2) from emp;
select MIN(hiredate),ename from emp group BY hiredate limit 1;
select sum(comm) from emp;
select deptno "部门",count(deptno) "人数" from emp GROUP BY deptno;
select deptno "部门",count(deptno) "人数", FORMAT(AVG(sal),2) "平均工资"from emp GROUP BY deptno;
select ename "员工",deptno"部门",MAX(sal)"最高工资" from emp where sal >8000 
GROUP BY deptno;
select deptno"部门编号",count(deptno)"人数" from emp GROUP BY deptno having 
COUNT(*) >2 ORDER BY deptno desc;
select * from emp where sal > (select avg(sal) from emp);
select * from emp where sal > (select max(sal) from emp where deptno = 1);
select * from emp where deptno = 1 and not ename = '小黑';                                                                                                                
select * from emp where not deptno = 1 and not ename = '小黑';
select * from emp a join dept b on a.deptno = b.deptno 
where loc ='上海';
select ename "员工姓名",CHAR_LENGTH(ename)"姓名长度" from emp;
select ename "员工姓名",SUBSTR(ename,-1,1) "最后一个字符串" from emp;
select CONCAT(dname,loc) from dept;
select * from emp where hiredate between '2022-1-1'and '2022-12-31';
select empno,ename,job,dname,loc from emp a join dept b 
on a.deptno = b.deptno;
select ename from dept a join emp b on a.deptno = b.deptno 
where loc = '上海' ORDER BY sal asc;
select a.ename "员工姓名",a.sal"工资",b.grade"工资等级" 
from emp a left join salgrade b on a.sal between b.losal and b.upsal;

select a1.empno"员工编号",a1.ename"员工姓名",a1.job"职位",a1.mgr"上级编号",
(select ename from emp a2 where a1.mgr = a2.empno) "上级领导姓名" 
from emp a1;

select e.ename,e.sal,s.grade from emp e left join salgrade s
on e.sal between s.losal and s.upsal;