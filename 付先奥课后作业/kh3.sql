-- 查询emp表中全部员工信息
select * from emp;
-- 查询emp表中的[员工姓名]、[工资]、[奖金]
select ENAME,SAL,COMM from emp ;
-- 查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
select EMPNO as '员工编号',ENAME as '姓名', HIREDATE as '入职时间' from emp;
-- 查询emp表中第3条开始，查询2条数据
select * from emp LIMIT 2,2;
-- 查询emp表中的全部数据，根据[入职时间]降序排序
select * from emp ORDER BY HIREDATE asc;
-- 查询emp表中工资最低的10个员工数据
select * from emp ORDER BY SAL  asc LIMIT 10;
-- 查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
select EMPNO,ENAME,SAL,HIREDATE from emp where SAL>6000;
-- 查询emp表中，职位为'后端开发'的员工信息
select * from emp where JOB='后端开发';
-- 查询emp表中，直系上级领导编号不是“2”的员工信息
select * from emp where MGR != 2 or MGR is NULL;
-- 查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
select EMPNO,ENAME,SAL from emp where SAL BETWEEN 7000 and 10000;
 asc;
-- 查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
select * from emp where  job like '%测试%' and SAL<=8000 ORDER BY EMPNO
-- 查询emp表中，工资大于6000，并且奖金不为空的员工信息
select * from emp where SAL>6000 and COMM is not null;
-- 查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
select * from emp where JOB='测试经理' or JOB='产品经理';
-- 查询emp表中，员工编号是2、4、5的员工信息
select * from emp where EMPNO in (2,4,5);
-- 查询emp表中，职位不是'前端开发'的员工信息
select * from emp where JOB != '前端开发';
-- 查询emp表中奖金为null的员工信息
select * from emp where COMM is null;
-- 查询emp表中有奖金的员工信息，且奖金不为0
select * from emp where COMM is not NULL and COMM!= 0;
-- 查询emp表中，员工姓名是‘黑’结尾的员工信息
select * from emp where ENAME like '%黑';
-- 查询emp表中，员工姓名包含‘红’字的员工信息
select * from emp where ENAME like '%红%';
-- 查询emp表中，员工姓名是两个字的员工信息
select * from emp where ENAME like '__';
-- 在emp表中，统计员工总人数
select count(EMPNO)as '员工人数' from emp ;
-- 在emp表中，统计员工平均工资
select avg(SAL)as '平均工资' from emp ;
-- 在emp表中，统计入职最早的[员工姓名]
select ENAME from emp ORDER BY HIREDATE asc limit 1;
-- 在emp表中，统计所有员工奖金总和
select sum(COMM) '总奖金' from emp;
-- 在emp表中，统计每个部门对应的员工人数
select DEPTNO,count(*) from emp GROUP BY DEPTNO;
-- 在emp表中，统计每个部门的员工人数，以及该部门的平均工资
select avg(SAL)as '平均工资',count(*) as '部门人数' from emp GROUP BY DEPTNO;
-- 在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
select EMPNO,DEPTNO as '部门',MAX(SAL) as '最大工资'from emp where SAL>8000  GROUP BY DEPTNO HAVING MAX(SAL);
-- 在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
select DEPTNO,count(*) from emp GROUP BY DEPTNO having COUNT(*) ORDER BY COUNT(*) asc;
-- 查询emp表中，工资大于平均工资的员工信息
select * from emp where SAL>(select avg(SAL)from emp);
-- 查询emp表中，工资大于1号部门最高工资的员工信息
select * from emp where SAL>(select MAX(SAL)from emp where DEPTNO=1 );
-- 查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
select * from emp where DEPTNO=(select DEPTNO from emp where ENAME = '小黑' );
-- 查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
select * from emp where DEPTNO !=(select DEPTNO from emp where ENAME = '小黑' );
-- 查询部门所在地区是'上海'的员工信息
select * from emp as e RIGHT JOIN dept as d on e.DEPTNO=d.DEPTNO where d.LOC='上海';
-- 查询emp表中，员工姓名以及姓名长度
select ENAME,CHAR_LENGTH(ENAME) from emp ;
-- 查询emp表中，员工的姓名以及姓名的最后一个字符
select ENAME,RIGHT(ENAME,1) from emp;
-- 查询dept表中，查询[部门名称]和[地址]的字符拼接内容
select concat(DNAME,LOC) from dept ;
-- 查询emp表中，入职时间是去年的员工信息
select * from emp WHERE HIREDATE BETWEEN '2022-01-01' and '2023-01-01';
-- 查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
SELECT EMPNO,ENAME,JOB,LOC,DNAME from emp as e INNER JOIN dept as d on e.DEPTNO=d.DEPTNO;
-- 查询所在地区在“上海”的员工有哪些，并根据工资升序排列
select ENAME from  emp as e INNER JOIN dept as d on e.DEPTNO=d.DEPTNO where LOC='上海' ORDER BY SAL DESC;
-- 查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
select e.ENAME as '员工姓名',e.SAL as '工资',s.Grade  as '工资等级' from emp as e JOIN salgrade as s on e.SAL BETWEEN s.LOSAL and s.UPSAL;
-- 查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
select a.EMPNO as '员工编号',a.ENAME as '员工名',a.JOB,b.EMPNO as '领导编号',b.ENAME as '领导名'  from emp as a LEFT JOIN emp as b on a.MGR = b.EMPNO;
