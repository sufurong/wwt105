CREATE TABLE comments (
	comments_id int PRIMARY key  auto_increment not null COMMENT'评论表id',
	comments_News_id int not null COMMENT'新闻',
	comment varchar(500) not null COMMENT'评论',
	comments_Create_by varchar(100) not null DEFAULT'18' COMMENT'创建人',
  comments_Create_date datetime not null  COMMENT'创建时间',
  comments_Update_by varchar(100) not null DEFAULT'19' COMMENT'更新人',
  comments_Update_date datetime not null  COMMENT'更新时间'
 )ENGINE=INNODB DEFAULT CHARSET=utf8;	
 desc comments;
 drop table comments;
 