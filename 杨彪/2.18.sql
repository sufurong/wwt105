CREATE TABLE `task_details` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `task_id` int NOT NULL COMMENT '任务id，关联task表的id',
  `work_id` int NOT NULL COMMENT '作品id（直播记录id、短视频id、图文资讯id）',
  `money_amount` double(8,2) DEFAULT NULL COMMENT '获得奖励额度',
  `finish_date` datetime DEFAULT NULL COMMENT '完成任务时间',
  `user_id` int NOT NULL COMMENT '用户编号',
  `is_del` int DEFAULT '0' COMMENT '是否删除，0.未删除 1.已删除',
  `create_user` int DEFAULT '0' COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` int DEFAULT '0' COMMENT '最后修改人',
  `update_date` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='任务明细表';


CREATE TABLE `task` (
  `task_id` int NOT NULL AUTO_INCREMENT COMMENT '任务编号',
  `task_type` int NOT NULL COMMENT '任务类型，枚举：1.主播任务 2.短视频任务 3.图文资讯任务',
  `task_title` varchar(255) DEFAULT NULL COMMENT '任务标题',
  `task_describe` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `task_profiles` varchar(255) DEFAULT NULL COMMENT '任务简介',
  `task_index` int DEFAULT NULL COMMENT '指标：1.开播时长  2.直播间人气   3.礼物收益   4.发布视频   5.视频点赞   6.视频转发',
  `task_condition` int DEFAULT NULL COMMENT '条件（单位为：分钟/人数/乐豆数/条数/点赞数/转发量）',
  `money_amount` double(8,2) DEFAULT NULL COMMENT '奖励额度',
  `task_toplimit` int DEFAULT NULL COMMENT '完成次数上限',
  `is_del` int DEFAULT '0' COMMENT '是否删除 0.未删除  1.已删除',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int DEFAULT '0' COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` int DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='任务表';


CREATE TABLE `live_details` (
  `live_details_id` int NOT NULL AUTO_INCREMENT COMMENT '直播记录编号',
  `live_room_id` int DEFAULT NULL COMMENT '直播间id',
  `live_title` varchar(255) DEFAULT NULL COMMENT '直播间主题',
  `live_picture_url` varchar(255) DEFAULT NULL COMMENT '主播间封面',
  `start_time` datetime DEFAULT NULL COMMENT '开播时间',
  `end_time` datetime DEFAULT NULL COMMENT '下播时间',
  `anchor_id` int DEFAULT NULL COMMENT '主播id',
  `is_del` int DEFAULT '0' COMMENT '是否删除 0.未删除  1.已删除',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int DEFAULT '0' COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` int DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`live_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='直播记录表';



CREATE TABLE `video_details` (
  `video_id` int NOT NULL AUTO_INCREMENT COMMENT '视频编号',
  `video_picture_url` varchar(255) DEFAULT NULL COMMENT '视频封面',
  `video_title` varchar(255) DEFAULT NULL COMMENT '视频标题',
  `video_url` varchar(255) DEFAULT NULL COMMENT '视频地址',
  `author_id` int DEFAULT NULL COMMENT '作者',
  `status` int DEFAULT '1' COMMENT '视频状态 1.待审核  2.审核通过  3.审核失败',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `review_time` datetime DEFAULT NULL COMMENT '审核时间',
  `is_del` int DEFAULT '0' COMMENT '是否删除，0.未删除 1.已删除',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int DEFAULT '0' COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` int DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='视频记录表';


CREATE TABLE `article_details` (
  `article_id` int NOT NULL AUTO_INCREMENT COMMENT '资讯编号',
  `article_title` varchar(255) DEFAULT NULL COMMENT '资讯标题',
  `article_content` text COMMENT '资讯内容',
  `author_id` int NOT NULL COMMENT '作者',
  `status` int DEFAULT '1' COMMENT '资讯状态 1.待审核  2.审核通过  3.审核失败',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `review_time` datetime DEFAULT NULL COMMENT '审核时间',
  `is_del` int DEFAULT '0' COMMENT '是否删除，0.未删除 1.已删除',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int DEFAULT '0' COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` int DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='图文资讯记录表';


CREATE TABLE `profit_statistics` (
  `profit_statistics_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int DEFAULT NULL COMMENT '用户编号',
  `statistics_time` datetime DEFAULT NULL COMMENT '统计时间',
  `task_type` int DEFAULT NULL COMMENT '任务类型，枚举：1.主播任务 2.短视频任务 3.图文资讯任务',
  `day_money_amount` double(8,2) DEFAULT NULL COMMENT '日收益额度',
  `is_del` int DEFAULT '0' COMMENT '是否删除，0.未删除 1.已删除',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int DEFAULT '0' COMMENT '创建人',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` int DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`profit_statistics_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='收益统计表';

insert into task(task_id,task_type,task_title,task_describe,task_profiles,task_index,task_condition,money_amount,task_toplimit,is_del,create_date,create_user,update_date,update_user)
 VALUES(
	4,1,'当时红极四时','当时红极四时','当时红极四时',4,1,100,2,0,'2023-2-18','3','2023-2-18','3'
)


insert into task_details(id,task_id,work_id,money_amount,finish_date,user_id,task_details.is_del,create_date,create_user,update_date,update_user)
VALUES(
	4,3,3,150,'2023-2-19',1,0,'2023-2-19','2','2023-2-20','2'
)

INSERT into live_details(live_details_id,live_room_id,live_title,live_picture_url,start_time,end_time,anchor_id,is_del,create_date,create_user,update_date,update_user)
VALUES(
		2,2,'当时红极','当时红极','2023-2-18','2023-2-19',3,0,'2023-2-19','1','2023-2-20','1'
)

INSERT into video_details(video_id,video_picture_url,video_title,video_url,author_id,`status`,publish_time,review_time,is_del,create_date,create_user,update_date,update_user)
VALUES(
	1,'当时红极','当时红极','当时红极',2,2,'2023-2-18','2023-2-19',0,'2023-2-19','2','2023-2-20','2'
)

INSERT INTO article_details(article_id,article_title,article_content,author_id,`status`,publish_time,review_time,is_del,create_date,create_user,update_date,update_user)
VALUES(
		1,'当时红极','当时红极',2,2,'2023-2-18','2023-2-19',0,'2023-2-19','2','2023-2-20','2'
	)




统计昨日收益
SELECT user_id as '用户编号',SUM(money_amount) as '总收益'
from task_details 
where finish_date= '2023-2-19'
GROUP BY user_id
统计累计收益

统计近7日折线图节点

统计单篇作品收益信息
















