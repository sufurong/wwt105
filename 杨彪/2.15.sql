 -- primry  主键
 -- auto_increment 主键自增
 -- comment 备注
 -- not null 非空
 -- unipue 唯一约束
 -- default "" 默认值
 
 drop table comments;
 
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论表id',
  `News_Id` int(11) NOT NULL COMMENT '新闻id',
  `comment` varchar(500) NOT NULL COMMENT '评论',
  `Create_by` varchar(100) NOT NULL COMMENT '创建人',
  `Create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `Update_by` varchar(100) NOT NULL COMMENT '更新人',
  `Update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

update comments  set comment="liisi" where id=1;

SELECT * from comments;
 