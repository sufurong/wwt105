CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newstitle` varchar(100) NOT NULL,
  `content` varchar(500) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT 1 NOT NULL ,
  `comment_num` int(11) DEFAULT 0 NULL ,
  `create_by` varchar(100) NOT NULL,
  `createdate` datetime NOT NULL,
  `update_by` varchar(100) NOT NULL,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 1.编写1条插入语句
INSERT INTO 
news (`newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) 
VALUES ('标题', '内容', 1, 1, 0, 'yb', '2011-1-1', 'yb', '2011-1-2')

-- 2．编写1条修改语句，将[新闻状态]为1的数据，[新闻标题]更新为“世界十大未解之谜”
update news set newstitle='世界十大未解之谜' where status=1

-- 3．编写1条删除语句，将[新闻标题]包含“世界”的数据删除
DELETE from news where newstitle LIKE '%世界%'

-- 4．查询[新闻内容]包含“经典”，[新闻标题]不包含“世界”的数据
SELECT * from news where content LIKE '%经典%' and not newstitle LIKE '%世界%'

-- 5.查询[评论数量]最多的10条的有效新闻
SELECT * from news where status=1 ORDER BY comment_num DESC  LIMIT 0,10

-- 6.查询[新闻标题]是8个字的，且至少已有一条评论的有效新闻
SELECT * from news 
where newstitle LIKE '________' 
and comment_num>0 
and status=1

-- 7.查询[创建时间]是2023年1月份的有效新闻
SELECT * from news where status=1 
and createdate BETWEEN '2023-1-1' and '2023-1-31'

-- 8．查询未录入[新闻内容]的无效数据
SELECT * from news where content is null and status=0

-- 9．查询[创建人]和[更新人]是同一人的新闻
SELECT * from news where create_by=update_by

-- 10.查询统计有效新闻和无效新闻的数量
SELECT status,COUNT(0) from news GROUP BY `status`

-- 11.查询在2022年下半年发布的新闻中，数量大于10条的[新闻类型]有哪些
SELECT type from news 
where createdate>'2022-6-30' and createdate<'2022-12-30'
GROUP BY type
HAVING count(0)>10

-- 12.计算每种[新闻类型]对应的平均评论数量
SELECT type,avg(comment_num) from news where GROUP BY type 