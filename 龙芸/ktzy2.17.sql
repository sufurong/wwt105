/*
 Navicat MySQL Data Transfer

 Source Server         : zuoye
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3306
 Source Schema         : ktzy2.17

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 20/02/2023 14:03:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for attendance
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '考勤数据编号',
  `UserId` int(11) NOT NULL COMMENT '学生编号',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `ClassHour` int(11) NOT NULL COMMENT '课时',
  `Date` datetime NOT NULL COMMENT '考勤日期',
  `SignIn` int(11) NULL DEFAULT NULL COMMENT '是否签到，0未签到 1签到',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of attendance
-- ----------------------------

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级编号',
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级名称',
  `Number` int(11) NOT NULL COMMENT '班级人数',
  `ClassHourCount` int(11) NOT NULL COMMENT '总课时',
  `CreateDate` datetime NOT NULL COMMENT '开班时间',
  `Grade` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后平均分等级：A/B/C/D/E',
  `IsEnd` int(11) NOT NULL COMMENT '是否结课：0未结课 1已结课',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of class
-- ----------------------------

-- ----------------------------
-- Table structure for salgrade
-- ----------------------------
DROP TABLE IF EXISTS `salgrade`;
CREATE TABLE `salgrade`  (
  `GRADE` int(11) NULL DEFAULT NULL COMMENT '工资等级',
  `LOSAL` int(11) NULL DEFAULT NULL COMMENT '最低工资',
  `UPSAL` int(11) NULL DEFAULT NULL COMMENT '最高工资'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of salgrade
-- ----------------------------

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '成绩单编号',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `UserId` int(11) NOT NULL COMMENT '学生编号',
  `Stage` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学习阶段',
  `Score` int(11) NULL DEFAULT NULL COMMENT '分数',
  `Remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of score
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生编号',
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生名称',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `Sex` int(11) NULL DEFAULT NULL COMMENT '性别，0女 1男',
  `Age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `Grade` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后考试等级：A/B/C/D/E',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
