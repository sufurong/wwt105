/*
 Navicat MySQL Data Transfer

 Source Server         : ly0216
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 20/02/2023 14:04:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `DEPTNO` int(11) NOT NULL COMMENT '部门编号',
  `DNAME` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `LOC` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门地址',
  PRIMARY KEY (`DEPTNO`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, '测试部', '上海');
INSERT INTO `dept` VALUES (2, '开发部', '北京');
INSERT INTO `dept` VALUES (3, '产品部', '杭州');
INSERT INTO `dept` VALUES (4, '客服部', '广州');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp`  (
  `EMPNO` int(11) NOT NULL COMMENT '员工编号',
  `ENAME` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `JOB` varchar(9) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位',
  `MGR` int(11) NULL DEFAULT NULL COMMENT '上级编号',
  `HIREDATE` date NULL DEFAULT NULL COMMENT '入职时间',
  `SAL` double NULL DEFAULT NULL COMMENT '工资',
  `COMM` double NULL DEFAULT NULL COMMENT '奖金',
  `DEPTNO` int(11) NULL DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`EMPNO`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES (1, '小白', '测试经理', NULL, '2019-11-22', 13000, 1000, 1);
INSERT INTO `emp` VALUES (2, '小黑', '测试组长', 1, '2022-09-06', 7000, NULL, 1);
INSERT INTO `emp` VALUES (3, '小蓝', '测试实习生', 2, '2022-11-02', 3000, NULL, 1);
INSERT INTO `emp` VALUES (4, '小红', '前端开发', NULL, '2022-08-30', 6000, 500, 2);
INSERT INTO `emp` VALUES (5, '小紫', '后端开发', NULL, '2021-08-11', 9000, 1000, 2);
INSERT INTO `emp` VALUES (6, '小绿', '产品经理', NULL, '2020-09-12', 17000, 2000, 3);

-- ----------------------------
-- Table structure for salgrade
-- ----------------------------
DROP TABLE IF EXISTS `salgrade`;
CREATE TABLE `salgrade`  (
  `GRADE` int(11) NULL DEFAULT NULL COMMENT '工资等级',
  `LOSAL` int(11) NULL DEFAULT NULL COMMENT '最低工资',
  `UPSAL` int(11) NULL DEFAULT NULL COMMENT '最高工资'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of salgrade
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
