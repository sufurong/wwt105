CREATE TABLE news(
id int not null primary key auto_increment  comment '新闻表id',
newstitle  varchar(100) not null comment '新闻标题',
content varchar(500) comment '新闻内容',
type int not null comment '新闻类型',
create_by varchar(100) not null comment '创建人',
createdate datetime not null comment '创建时间',
update_by varchar(100) not null comment '更新人',
updatedate datetime not null comment '更新时间'
);


alter table news add status int not null comment '新闻状态';
alter table news add comment_num int not null comment '新闻数量';

select * from news;


-- 1、编写一条插入语句
insert into news values(1,'汪松教授获爱丁堡科学奖',
'中国科学院动物所的汪松教授上月在英国爱丁堡市接受了世界著名的爱丁堡科学奖',1,1,0,'小嘉','2023-01-16','小方','2023-02-16');
insert into news values(2,'世界未解之谜',
'世界未解之谜世界未解之谜世界未解之谜',1,1,0,'小白','2022-01-15','小黑','2023-02-11'),
(3,'练习题第四条',
'世界未解之谜世界未解之谜世界未解之谜',1,1,0,'小左','2020-03-15','小右','2022-08-11')
;

insert into news values(4,'新闻标题1',
'新闻内容1',1,1,1,'小1','2023-01-16','小2','2023-02-16');

insert into news values(12,'新闻标题1',
'新闻内容1',1,1,1,'小1','2023-01-16','小2','2023-02-16'),
(5,'新闻标题2',
'新闻内容2',1,1,3,'小3','2023-01-16','小4','2023-02-16'),
(6,'新闻标题3',
'新闻内容3',1,1,5,'小左','2023-01-16','小右','2023-02-16'),
(7,'新闻标题4',
'新闻内容4',1,1,7,'小左','2023-01-16','小右','2023-02-16'),
(8,'新闻标题5',
'新闻内容5',1,1,8,'小左','2023-01-16','小右','2023-02-16'),
(9,'新闻标题6',
'新闻内容6',1,1,10,'小左','2023-01-16','小右','2023-02-16'),
(10,'新闻标题7',
'新闻内容7',1,1,20,'小左','2023-01-16','小右','2023-02-16'),
(11,'新闻标题8',
'新闻内容8',1,1,50,'小左','2023-01-16','小右','2023-02-16')

;

insert into news values(13,'新闻标题9',
'新闻内容9',1,0,50,'小1','2023-01-16','小2','2023-02-16');
insert into news values(14,'新闻标题10',
'新闻内容10',2,1,100,'小1','2023-01-16','小2','2023-02-16'),
(15,'新闻标题11',
'新闻内容11',2,1,100,'小1','2023-01-16','小2','2023-02-16'),
(16,'新闻标题13',
'新闻内容13',2,1,150,'小1','2022-08-16','小2','2022-010-16')
;





update news set content='这就是经典' where id=3;
update news set create_by='小方' where id=1;
update news set content='' where id=13;



-- 2、 填写一条修改语句，将新闻状态为1的数据，新闻标题更新为’世界十大未解之谜‘
update  news set newstitle = '世界十大未解之谜' where status=1;

-- 3、编写一条删除语句，将新闻标题包含世界的数据删除
delete  from news where newstitle like '%世界%' ;

-- 4、查询新闻内容包含经典，新闻标题不包含世界的数据
select * from news where content like '%经典%' and not newstitle like '%世界%';

-- 5、查询评论数量最多的10条的有效新闻
select * from news where status =1 order by comment_num desc limit 0,10;

-- 6、查询新闻标题是8个字的，且至少已有一条评论的有效新闻
select  * from news where status=1 and newstitle like '________' and comment_num >=1;

-- 7、查询创建时间是2023年一月份的有效新闻
select * from news where createdate like '2023-01-%' and status=1;

-- 8、查询创建人和更新人是同一人的新闻
select * from news where Create_by=Update_by;

-- 9、查询未录入新闻内容的无效数据
select * from news where content is null and status=0;
 
-- 10、查询统计有效新闻和无效新闻的数量
select  status ,count(status) from news GROUP BY status;

-- 11、查询在2022年下半年发布的新闻中，数量大于10条的新闻类型有哪些

select type from news where createdate BETWEEN'2022-07-01' AND '2022-12-31' group by type having count(*)>10;

-- 12、计算每种新闻类型对应的平均评论数量
select type ,avg(comment_num) 平均数量 from news group by type ;

