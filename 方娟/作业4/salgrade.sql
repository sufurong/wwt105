/*
 Navicat MySQL Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3307
 Source Schema         : user

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 20/02/2023 14:38:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for salgrade
-- ----------------------------
DROP TABLE IF EXISTS `salgrade`;
CREATE TABLE `salgrade`  (
  `GRADE` int(11) NULL DEFAULT NULL COMMENT '工资等级',
  `LOSAL` int(11) NULL DEFAULT NULL COMMENT '最低工资',
  `UPSAL` int(11) NULL DEFAULT NULL COMMENT '最高工资'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of salgrade
-- ----------------------------
INSERT INTO `salgrade` VALUES (1, 0, 1999);
INSERT INTO `salgrade` VALUES (2, 2000, 4999);
INSERT INTO `salgrade` VALUES (3, 5000, 9999);
INSERT INTO `salgrade` VALUES (4, 10000, 20000);

SET FOREIGN_KEY_CHECKS = 1;
