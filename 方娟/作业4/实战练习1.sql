SELECT * FROM `salgrade`

SELECT * FROM emp
SELECT * FROM dept
desc salgrade;

INSERT INTO salgrade VALUES(1,0,1999),(2,2000,4999),(3,5000,9999),(4,10000,20000);

--  1、查询emp表中全部员工信息
SELECT *FROM emp;

--  2、查询emp表中的[员工姓名]、[工资]、[奖金]
SELECT ENAME,SAL,COMM FROM emp;
-- 3、查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
SELECT EMPNO 员工编号,ENAME 员工姓名, HIREDATE 入职时间 FROM emp;

-- 4、查询emp表中第3条开始，查询2条数据
SELECT * FROM emp LIMIT 2,2;

-- 5、查询emp表中的全部数据，根据[入职时间]降序排序
SELECT * FROM emp ORDER BY HIREDATE DESC;

-- 6、查询emp表中工资最低的10个员工数据
SELECT * FROM emp ORDER BY SAL ASC LIMIT 0,10;

-- 7、查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
SELECT EMPNO , ENAME , SAL , HIREDATE FROM emp WHERE SAL > 6000;

-- 8、查询emp表中，职位为'后端开发'的员工信息
SELECT * FROM emp WHERE JOB = '后端开发';

-- 9、查询emp表中，直系上级领导编号不是“2”的员工信息
SELECT * FROM emp WHERE MGR !=2;

-- 10、查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
SELECT EMPNO , ENAME , SAL FROM emp WHERE SAL BETWEEN 7000 AND 10000;

-- 11、查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
 SELECT * FROM emp WHERE JOB LIKE '%测试%' AND SAL <= 8000 ORDER BY EMPNO DESC;
 
-- 12、查询emp表中，工资大于6000，并且奖金不为空的员工信息
SELECT * FROM emp WHERE SAL > 6000 AND COMM IS NOT NULL;

-- 13、查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
SELECT * FROM emp WHERE JOB = '测试经理' OR '产品经理';

-- 14、查询emp表中，员工编号是2、4、5的员工信息
SELECT * FROM emp WHERE EMPNO IN (2,4,5);

-- 15、查询emp表中，职位不是'前端开发'的员工信息
SELECT * FROM emp WHERE JOB != '前端开发';

-- 16、查询emp表中奖金为null的员工信息
SELECT * FROM emp WHERE COMM IS NULL;

-- 17、查询emp表中有奖金的员工信息，且奖金不为0
SELECT * FROM emp WHERE COMM IS NOT NULL AND COMM !=0;

-- 18、查询emp表中，员工姓名是‘黑’结尾的员工信息
SELECT * FROM emp WHERE ENAME LIKE '%黑';

-- 19、查询emp表中，员工姓名包含‘红’字的员工信息
SELECT * FROM emp WHERE ENAME LIKE '%红%';

-- 20、查询emp表中，员工姓名是两个字的员工信息
SELECT * FROM emp WHERE ENAME LIKE '__';

-- 21、在emp表中，统计员工总人数
SELECT COUNT(*) FROM emp;

-- 22、在emp表中，统计员工平均工资
SELECT AVG(SAL) FROM emp;

-- 23、在emp表中，统计入职最早的[员工姓名]
SELECT ENAME FROM emp ORDER BY HIREDATE LIMIT 0,1;

-- 24在emp表中，统计所有员工奖金总和
SELECT SUM(COMM) FROM emp ;

-- 25在emp表中，统计每个部门对应的员工人数
SELECT DEPTNO , COUNT(*) FROM emp GROUP BY JOB;

-- 26在emp表中，统计每个部门的员工人数，以及该部门的平均工资
SELECT DEPTNO , COUNT(*) , AVG(SAL) FROM emp GROUP BY JOB ;

-- 27在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
SELECT ENAME , DEPTNO , MAX(SAL) FROM emp WHERE SAL > 8000 GROUP BY  DEPTNO;

-- 28在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列 
SELECT DEPTNO , COUNT(*) FROM emp  GROUP BY DEPTNO HAVING COUNT(*) > 2 ORDER BY COUNT(*) DESC;

-- 29查询emp表中，工资大于平均工资的员工信息
SELECT * FROM emp  WHERE SAL > (SELECT AVG(SAL) FROM emp);

-- 30查询emp表中，工资大于1号部门最高工资的员工信息
SELECT MAX(SAL) FROM emp WHERE DEPTNO = 1;
SELECT * FROM emp WHERE SAL > (SELECT MAX(SAL) FROM emp WHERE DEPTNO = 1);

-- 31查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
SELECT DEPTNO FROM emp WHERE ENAME = '小黑';
SELECT * FROM emp WHERE DEPTNO = (SELECT DEPTNO FROM emp WHERE ENAME = '小黑');

-- 32查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
SELECT * FROM emp WHERE DEPTNO != (SELECT DEPTNO FROM emp WHERE ENAME = '小黑');

-- 33查询部门所在地区是'上海'的员工信息
SELECT * FROM emp JOIN dept ON emp.DEPTNO = dept.DEPTNO WHERE LOC = '上海';

-- 34查询emp表中，员工姓名以及姓名长度
SELECT ENAME , CHAR_LENGTH(ENAME) FROM emp;

-- 35查询emp表中，员工的姓名以及姓名的最后一个字符
SELECT ENAME , RIGHT(ENAME,1) FROM emp;

-- 36查询dept表中，查询[部门名称]和[地址]的字符拼接内容
SELECT CONCAT(DNAME,LOC) FROM dept;

-- 37查询emp表中，入职时间是去年的员工信息
SELECT * FROM emp WHERE HIREDATE < '2023-01-01';

-- 38查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
SELECT EMPNO , ENAME , JOB , DNAME , LOC FROM emp JOIN dept ON emp.DEPTNO=dept.DEPTNO;

-- 39查询所在地区在“上海”的员工有哪些，并根据工资升序排列\
SELECT * FROM emp JOIN dept ON emp.DEPTNO=dept.DEPTNO WHERE dept.LOC='上海' ORDER BY SAL ASC;

-- 40查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
SELECT ENAME 员工姓名, SAL 工资, GRADE 工资等级 FROM emp JOIN salgrade ON emp.SAL BETWEEN LOSAL AND UPSAL ;

-- 41查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
SELECT e1.EMPNO 员工编号, e1.ENAME 员工姓名 ,e1.JOB 职位 ,e1.MGR 上级领导编号 ,e2.ENAME 上级领导姓名 FROM emp e1 JOIN emp e2 ON e1.MGR=e2.EMPNO;
