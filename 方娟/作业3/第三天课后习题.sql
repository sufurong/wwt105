UPDATE score set Score=NULL WHERE Id=1;
SELECT * FROM score;

-- 1、查询班级名称为WWWT105的班级，在2023年2月15日未签到的学生名单
-- SELECT u.Id,u.Name,u.ClassId,Sex,Age,Grade,a.Date,a.SignIn  FROM attendance a 
-- JOIN user u 
-- ON a.UserId=u.Id 
-- WHERE (a.Date BETWEEN '2023-02-15 00:00:00' AND '2023-02-16 00:00:00') AND a.SignIn=0; 
-- 
-- 
-- SELECT Id ,Name FROM class WHERE  class.Name='WWWT105';
-- 
-- 
-- SELECT H.Id,H.Name,H.ClassId,Sex,Age,Grade,Date,SignIn,Q.Name 
-- FROM (SELECT u.Id,u.Name,u.ClassId,Sex,Age,Grade,a.Date,a.SignIn FROM attendance a 
-- JOIN user u 
-- ON a.UserId=u.Id 
-- WHERE (a.Date BETWEEN '2023-02-15 00:00:00' AND '2023-02-16 00:00:00') AND a.SignIn=0) H
-- JOIN (SELECT Id,Name FROM class WHERE  class.Name='WWWT105') Q ON H.ClassId=Q.Id;
查询班级名称为WWWT105的班级，在2023年2月15日未签到的学生名单
SELECT u.name FROM attendance a
JOIN user u
ON a.UserId=u.Id
JOIN class c
ON a.classId=c.Id
WHERE a.Date BETWEEN '2023-02-15 00:00:00' AND '2023-02-16 00:00:00'
AND a.SignIn=0
AND c.Name='WWWT105';



-- 2、查询班级名称为WWWT105的班级的男女比例
-- SELECT ClassId ,SUM(case when sex=1 then 1 else 0 end) s1 ,SUM(case when sex=0 then 1 else 0 end) s2 FROM user;
-- SELECT Id ,Name FROM class WHERE  class.Name='WWWT105';
-- 
-- SELECT s1 div s2 男女比例
-- FROM (SELECT ClassId,SUM(case when sex=1 then 1 else 0 end) s1 ,SUM(case when sex=0 then 1 else 0 end) s2 FROM user) A
-- JOIN (SELECT Id ,Name FROM class WHERE  class.Name='WWWT105') B
-- on A.ClassId=B.Id;
-- 
SELECT SUM(case when sex=1 then 1 else 0 end)/SUM(case when sex=0 then 1 else 0 end)
FROM user ,class 
WHERE class.Name='WWWT105';



-- 3、查询班级名称为WWWT105的班级， 在学习阶段为“功能测试阶段”，未参加考试的名单（即，分数=Null）
-- SELECT * FROM class WHERE  class.Name='WWWT105';
-- 
-- SELECT u.Id,u.Name FROM (SELECT * FROM class WHERE  class.Name='WWWT105') s 
-- JOIN user u
-- ON s.Id=u.Id;
-- 
-- SELECT userId,Stage,Score FROM score WHERE Stage='初级' AND Score is NULL;
-- SELECT * FROM score;
-- 
-- SELECT * FROM (SELECT userId,Stage,Score FROM score WHERE Stage='初级' AND Score is NULL) E
-- JOIN (SELECT u.Name FROM (SELECT * FROM class WHERE  class.Name='WWWT105') s 
-- JOIN user u
-- ON s.Id=u.Id) F
-- ON E.UserId=F.Id;
-- 
SELECT u.Name
FROM user u
JOIN score s
on u.Id=s.UserId
JOIN class c
ON s.classId=c.Id
WHERE c.Name = 'WWWT105'
AND s.Stage ='初级'
AND s.Score is null;



-- 4、查询2023年2月1日， 全员都签到的班级名称
SELECT a.ClassId, COUNT(a.Id),c.Name
FROM attendance a 
JOIN class c
ON a.ClassId=c.Id
WHERE a.Date ='2023-02-01 00:00:00'
AND a.SignIn=1
GROUP BY a.ClassId
HAVING COUNT(*)=(SELECT Number FROM class WHERE Id=a.ClassId);

-- 5、查询全校的男生平均年龄、女生平均年龄、全校平均年龄
SELECT '男平均' as '分组', avg(u.Age) as '年龄' FROM `user` u where u.Sex = 1
UNION
SELECT '女平均', avg(u.Age) FROM `user` u where u.Sex = 0
UNION
SELECT '校平均', avg(u.Age) FROM `user` u;


-- 6、查询班级名称为WWWT105的班级，成绩ABCDE每个等级的人数分别有所少
SELECT COUNT(U.Id), U.Grade FROM `user` U
LEFT JOIN class c
ON U.ClassId = C.Id
WHERE c.`Name` = 'WWWT105'
GROUP BY U.Grade

-- 7、查询班级名称WWWT105班级里，名为“阿龙”的同学参加考试的所有成绩单平均分
SELECT avg(s.score) FROM `user` U
INNER JOIN class C
ON U.ClassId = C.Id
INNER JOIN score s
on s.UserId = u.Id
WHERE C.`Name` = 'WWWT105'
and U.`Name` = '熊二';

-- 8、假设分数100-90=A级、89-75=B级、74-60=C级、59-45=D级、44-0=E级，输录入WWWT105的阿龙同学成绩单，
并更新相关表（需要更新score成绩表、user学生用户表、class班级表）
DROP PROCEDURE if exists add_score; -- 删除存储函数

delimiter $$
CREATE PROCEDURE add_score(in className VARCHAR(50), in userName VARCHAR(50), in StageName VARCHAR(50), in addScore int)
BEGIN
DECLARE cid int;	-- 班级编号
DECLARE uid int;	-- 用户编号
set cid = (SELECT Id FROM class C WHERE C.`Name` = className);
set uid = (SELECT u.Id FROM class c, `user` u where c.Id = u.ClassId and C.`Name` = className AND u.`Name` = userName);
INSERT into score VALUES(DEFAULT, cid, uid, StageName, addScore, NULL);

UPDATE `user` SET GRADE = 
(SELECT case
	when Score<=100 and Score>=90 then 'A'
	when Score<=89 and Score>=75 then 'B'
	when Score<=74 and Score>=60 then 'C'
	when Score<=59 and Score>=45 then 'D'
	when Score<=44 and Score>=0 then 'E'
	ELSE
		'其他'
END '成绩等级' FROM score WHERE UserId = uid ORDER BY id desc limit 1 )
 WHERE id = uid;

UPDATE class set GRADE = 
(SELECT case
	when avg(Score) >=90 then 'A'
	when avg(Score) >=75 then 'B'
	when avg(Score) >=60 then 'C'
	when avg(Score) >=45 then 'D'
	when avg(Score) >=0 then 'E'
	ELSE '其他'
	END'平均分' FROM score WHERE ClassId = cid)
WHERE id = cid;

END $$
delimiter;


CALL add_score('WWT105', '喵小米', '接口测试阶段', 99);

