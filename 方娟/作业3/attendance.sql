/*
 Navicat MySQL Data Transfer

 Source Server         : 本地MySQL
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3307
 Source Schema         : user

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 20/02/2023 14:35:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for attendance
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '考勤数据编号',
  `UserId` int(11) NOT NULL COMMENT '学生编号',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `ClassHour` int(11) NOT NULL COMMENT '课时',
  `Date` datetime NOT NULL COMMENT '考勤日期',
  `SignIn` int(11) NULL DEFAULT NULL COMMENT '是否签到，0未签到 1签到',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of attendance
-- ----------------------------
INSERT INTO `attendance` VALUES (1, 1, 1, 20, '2023-02-17 14:54:54', 1);
INSERT INTO `attendance` VALUES (2, 2, 1, 20, '2023-02-17 14:54:24', 1);
INSERT INTO `attendance` VALUES (3, 3, 1, 20, '2023-02-17 14:54:53', 1);
INSERT INTO `attendance` VALUES (4, 4, 1, 20, '2023-02-15 12:12:12', 0);
INSERT INTO `attendance` VALUES (5, 5, 1, 20, '2023-02-17 14:52:37', 0);
INSERT INTO `attendance` VALUES (6, 6, 1, 20, '2023-02-17 14:50:53', 1);

SET FOREIGN_KEY_CHECKS = 1;
