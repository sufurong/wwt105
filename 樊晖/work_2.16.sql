/*
 Navicat MySQL Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3306
 Source Schema         : work_2.16

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 16/02/2023 17:02:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '新闻表id',
  `newstitle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '新闻标题',
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新闻内容',
  `type` int(11) NOT NULL COMMENT '新闻类型',
  `status` int(11) NOT NULL COMMENT '新闻状态',
  `comment_num` int(11) NULL DEFAULT NULL COMMENT '评论数量',
  `create_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `createdate` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新人',
  `updatedate` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (1, '震惊！', '英雄联盟', 2, 0, 99, '小白', '2023-02-16 15:45:00', '小黑', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (3, '哈哈！', '经典', 1, 0, 88, '小白', '2023-02-16 15:45:00', '小黑', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (5, '嘻嘻！', '艾欧尼亚', 1, 0, 1000, '小白', '2023-02-16 15:45:00', '小白', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (7, '嘿嘿！', '符文大陆', 1, 0, 9999, '小黑', '2023-02-16 15:45:00', '小黑', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (8, '呜呜！', '德玛西亚', 1, 0, 4989, '小红', '2023-02-16 15:45:00', '小蓝', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (10, '啦啦！', '黑玫瑰', 1, 0, 59, '小蓝', '2023-02-16 15:45:00', '小红', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (12, '娃哈哈！', '无极村', 1, 0, 9890, '小蓝', '2023-02-16 15:45:00', '小蓝', '2023-02-17 15:45:00');
INSERT INTO `news` VALUES (14, '小白小红小黑小蓝', '蓝精灵', 1, 0, 1, '小蓝', '2022-02-16 15:45:00', '小蓝', '2022-02-17 15:45:00');
INSERT INTO `news` VALUES (15, '娃哈哈蓝精灵', '蓝精灵', 1, 0, 1, '小蓝', '2022-02-16 15:45:00', '小蓝', '2022-02-17 15:45:00');
INSERT INTO `news` VALUES (17, '果汁', '橙汁', 1, 0, 1010, '小白', '2022-01-22 15:45:00', '小红', '2022-01-25 15:45:00');
INSERT INTO `news` VALUES (18, '果汁', '', 1, 0, 10, '小白', '2022-01-22 15:45:00', '小白', '2022-01-25 15:45:00');
INSERT INTO `news` VALUES (19, '果汁', NULL, 1, 0, 10, '小白', '2022-01-22 15:45:00', '小白', '2022-01-25 15:45:00');
INSERT INTO `news` VALUES (20, '果汁', '西瓜汁', 1, 0, 1010, '小白', '2022-07-22 15:45:00', '小红', '2022-08-25 15:45:00');
INSERT INTO `news` VALUES (23, '果汁', '草莓汁', 1, 0, 1010, '小白', '2023-01-22 15:45:00', '小红', '2023-01-25 15:45:00');
INSERT INTO `news` VALUES (24, '果汁', '橙汁', 2, 1, 1010, '小白', '2023-01-11 15:45:00', '小红', '2023-01-12 15:45:00');

SET FOREIGN_KEY_CHECKS = 1;
