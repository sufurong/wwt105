--  1. 查询班级名称为WWT105的班级，在2023年2月15日未签到的学生名单
select u.`name` from attendance a
left join class c
on a.classid = c.id
left join `user` u
on a.userid = u.id
where a.date = '2023-02-01'
and a.signin = 0
and c.`name` = 'WWT105';

--  2. 查询班级名称为WWT105的班级的男女比例
select sum(case when sex=1 then 1 else 0 end)/sum(case when sex=0 then 1 else 0 end)
from `user`u,class c
where u.classid = c.id
and c.`name` = 'WWT105';

--  3. 查询班级名称为WWT105的班级，在学习阶段为“功能测试阶段”，未参加考试的名单（即，分数=null）
select u.`name`from score s
left join `user` u
on s.userid = u.id
left join class c
on c.id = s.classid
where c.`name` = 'WWT105'
and s.stage = '功能测试阶段'
and s.score is null;


--  4. 查询2023年2月1日，全员都签到的班级名称
select a.classid,count(a.id),c`name` from attendance a
inner join class c
on c.id = a.classid
where a.date = '2023-02-01'
and a.signin = 1
group by a.classid
having count(*) = (select number from class where id = a.classid);

--  5. 查询全校的男生平均年龄、女生平均年龄、全校平均年龄
select '男平均' as '分组',avg(u.age) as '年龄' from `user` u where u.sex = 1
union
select '女平均',avg(u.age) from `user` u where u.sex = 0
union
select '校平均',avg(u.age)from `user` u;

--  6. 查询班级名称为WWT105的班级，成绩ABCDE每个等级的人数分别有多少
select count(u.id),u.grade from `user` u
left join class c
on u classid = c.id
where c.`name` = 'WWT105'
group by u.grade

--  7. 查询班级名称WWT105班级里，名为“阿龙”的同学参加考试的所有成绩单平均分
select avg(s.score)from `user` u
inner join class c
on u.classid = c.id
inner join score s
on s.userid = u.id
where c.`name` = 'WWT105'
and u.`name` = '阿龙';
