--  1查询emp表中全部员工信息  select * from 表名；
select * from emp
--  2查询emp表中的员工姓名，工资，奖金  select name, age from user_info;
select ename, sal,comm from emp;
--  3查询emp表中的员工编号，员工姓名，入职时间，并给每一列起中文别名显示  select name as '姓名' from user_info;
select empno as '员工编号' from emp;
select ename as '员工姓名' from emp;
select hiredate as '入职时间' from emp;
--  4查询emp表中第三条开始，查询2条数据  select * from user_info limit 0,10;
select * from emp limit 0,2;
--  5查询emp表中的全部数据，根据入职时间降序排列  select * from user_info order by age desc;
select * from emp order by hiredate desc;
--  6查询emp表中工资最低的十个员工数据  select * from user_info order by age desc limit 5, 5;
select * from emp order by sal asc limit 0,10;
--  7查询emp表中，工资大于6000的员工编号，姓名，工资，入职时间  select * from 表名 where 条件;
select * from emp where sal >6000;
--  8查询emp表中，职位为后端开发的员工信息
select * from emp where job ='后端开发';
--  !9查询emp表中，直系上级领导编号不是2的员工信息
select * from emp where not mgr = '2';
--  10查询emp表中，工资范围在7000-10000之间的员工编号，姓名，工资  select * from students where age between 20 and 25;
select * from emp where sal between 7000 and 10000;
--  11查询emp表中，职位包含测试关键词，并且工资小于等于8000发员工信息，按照员工编号降序排列
select * from emp where job like '测试%' and sal  <=8000;
--  12查询emp表中，工资大于6000，并且奖金不为空的员工信息
select * from emp where sal >6000 and comm is not null;
--  13查询emp表中，职位是测试经理或者职位是产品经理的员工信息
select * from emp where job = '测试经理' or '产品经理';
--  14查询emp表中，员工编号是2，4，5的员工信息
select * from emp where deptno in(2,4,5);
--  15查询emp表中，职位不是前端开发的员工信息
select * from emp where not job ='前端开发';
--  16查询emp表中奖金为null的员工信息
select * from emp where comm is null;
--  17查询emp表中有奖金的员工信息，且奖金不为0
select * from emp where comm is not null;
--  18查询emp表中员工姓名是‘黑’结尾的员工信息
select * from emp where ename like '%黑';
--  19查询emp表中员工名字包含‘红’字的员工信息
select * from emp where ename like '%红%';
--  20查询emp表中，员工姓名是两个字的员工信息
select * from emp where ename like '__';
--  21在emp表中统计员工总人数  count()统计行数
select count(ename)  from emp;
--  22在emp表中统计员工平均工资  avg()求平均值
select avg(sal) from emp;
--  23在emp表中统计入职最早的员工姓名  min()最小值
select min(hiredate) from emp;
--  24在emp表中，统计所有员工奖金总和  sum()求和
select sum(comm) from emp;
--  25在emp表中，统计每个部门对应的员工人数
SELECT
 * 
FROM
 emp AS e,
 dept AS d 
WHERE
 e.DEPTNO = d.deptno
--  26在emp表中，统计每个部门的员工人数，以及该部门的平均工资
select deptno,count(*),avg(sal) from emp group by deptno;
--  27在emp表中，统计工资大于8000的员工，分布在那些部门，且该部门最高的工资是多少
select deptno, max(sal) from emp where sal > 8000 group by deptno;
--  28在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
select deptno from emp group by deptno having count(*)> 2 order by count(*) desc;
--  29在emp表中，工资大于平均工资的员工信息
select * from emp where sal >(select avg(sal) from emp);
--  30在emp表中，工资大于1号部门最高工资的员工信息
select * from emp where sal > (select max(sal) from emp where deptno=1);
--  31查询emp表中，与姓名叫小黑是同一个部门的其他员工信息
select * from emp where deptno = (select deptno from emp where ename= '小黑' ) and ename != '小黑';
--  32查询emp表中，与姓名叫小黑不是同一个部门的员工信息
select * from emp where deptno != (select deptno from emp where ename = '小黑');
--  33查询部门所在地区是上海的员工信息
select e.*from emp e
inner join dept d
on e.deptno = d.deptno
where d.loc = '上海';
--  34查询emp表中，员工姓名以及姓名长度
select ename,length(ename) from emp;
--  35查询emp表中，员工的姓名以及姓名的最后一个字符
select ename,substr(ename,-1,1) from emp;
--  36查询dept表中，查询[部门名称]和[地址]的字符拼接内容
select concat(dname,loc) from dept;
--  37查询emp表中，入职时间是去年的员工信息
select * from emp where year (hiredate) = year(now())-1;
-- 38查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
select e.empno, e.ename, e.job, d.dname, d.loc from emp e
left join dept d
on e.deptno = d.deptno;
--  39查询所在地区在“上海”的员工有哪些，并根据工资升序排列
select e.*from emp e
left join dept d
on e.deptno = d.deptno
where d.loc = '上海'
order by e.sal asc;
--  40查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
SELECT e.ename '员工姓名', e.sal '工资', s.grade '工资等级' from emp e
left JOIN salgrade s
on e.sal between s.losal and s.upsal;
--  41查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
select e1.empno, e1.ename, e1.job e2.empno, e2.ename
from emp e1,emp e2
where e1.mgr = e2.empno;