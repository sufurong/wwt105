1.查询emp表中全部员工信息
 SELECT * FROM emp;
2.查询emp表中的[员工姓名]、[工资]、[奖金]
 select ENAME,SAL,COMM from emp;
3.查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
 SELECT EMPNO 'a',ENAME 'b',HIREDATE 'c' FROM emp;
4.查询emp表中第3条开始，查询2条数据
SELECT * FROM emp LIMIT 2,2;
5.查询emp表中的全部数据，根据[入职时间]降序排序
SELECT * FROM emp ORDER BY HIREDATE desc;
6.查询emp表中工资最低的10个员工数据
SELECT * FROM emp ORDER BY sal asc LIMIT 0,10;
7.查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
SELECT EMPNO '员工编号', ENAME '员工姓名', SAL '工资', HIREDATE '入职时间' FROM emp where SAL > 6000;
8.查询emp表中，职位为'后端开发'的员工信息
SELECT * FROM emp where JOB LIKE '后端开发';
9.查询emp表中，直系上级领导编号不是“2”的员工信息
SELECT * FROM emp WHERE MGR!=2;
10.查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
SELECT EMPNO '员工编号', ENAME '员工姓名', SAL '工资' FROM emp WHERE SAL BETWEEN 7000 AND 10000;
11.查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
SELECT * FROM emp WHERE JOB LIKE '%测试%' AND SAL <= 8000  ORDER BY EMPNO desc;
12.查询emp表中，工资大于6000，并且奖金不为空的员工信息
SELECT *FROM emp where SAL > 6000 AND COMM is NOT NULL;
13.查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
SELECT * FROM emp WHERE (JOB like '测试经理') OR (JOB LIKE '产品经理');
14.查询emp表中，员工编号是2、4、5的员工信息
SELECT * FROM emp WHERE EMPNO IN(2,4,5);
15.查询emp表中，职位不是'前端开发'的员工信息
 SELECT * FROM emp where JOB != '前端开发';
16.查询emp表中奖金为null的员工信息
 SELECT * FROM emp WHERE COMM is NULL;
17.查询emp表中有奖金的员工信息，且奖金不为0
 SELECT * FROM emp WHERE comm IS NOT NULL AND comm != 0;
18.查询emp表中，员工姓名是‘黑’结尾的员工信息
 SELECT * FROM emp WHERE ename like '%黑';
19.查询emp表中，员工姓名包含‘红’字的员工信息
SELECT * FROM emp WHERE ename LIKE '%红%';
20.查询emp表中，员工姓名是两个字的员工信息
SELECT * FROM emp WHERE ename LIKE '__';
21.在emp表中，统计员工总人数
SELECT count(EMPNO) FROM emp;
22.在emp表中，统计员工平均工资
SELECT avg(sal) FROM emp;
23.在emp表中，统计入职最早的[员工姓名]
SELECT ename,HIREDATE from emp ORDER BY HIREDATE asc LIMIT 1;
24.在emp表中，统计所有员工奖金总和
SELECT sum(comm) FROM emp;
25.在emp表中，统计每个部门对应的员工人数
1- SELECT dept.dname 部门名称,count(empno) 部门人数 from emp INNER JOIN dept on emp.DEPTNO = dept.DEPTNO GROUP BY emp.DEPTNO;
2- SELECT DEPTNO,count(ename) FROM emp GROUP BY DEPTNO;
26.在emp表中，统计每个部门的员工人数，以及该部门的平均工资
1- SELECT dept.dname 部门名称,count(empno) 部门人数,avg(sal) 平均工资 from emp INNER JOIN dept on emp.DEPTNO = dept.DEPTNO GROUP BY emp.DEPTNO;
2- SELECT DEPTNO,count(ename),avg(sal) FROM emp GROUP BY DEPTNO;
27.在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
SELECT * FROM emp
INNER JOIN dept
on dept.DEPTNO=emp.DEPTNO
where sal > 8000
ORDER BY emp.SAL desc;
28.在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
SELECT DEPTNO FROM emp
GROUP BY DEPTNO
having count(DEPTNO)>2
ORDER BY DEPTNO DESC;
29.查询emp表中，工资大于平均工资的员工信息
SELECT * FROM emp WHERE sal > (SELECT avg(sal) FROM emp);
30.查询emp表中，工资大于1号部门最高工资的员工信息
SELECT * FROM emp WHERE sal > (SELECT sal FROM emp WHERE deptno = 1 ORDER BY sal DESC LIMIT 0,1)
31.查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
SELECT * FROM emp WHERE DEPTNO=(SELECT DEPTNO FROM emp WHERE ENAME='小黑') AND ENAME !='小黑';
32.查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
SELECT * FROM emp WHERE NOT deptno = (SELECT deptno FROM emp WHERE ename = '小黑');
33.查询部门所在地区是'上海'的员工信息
SELECT * FROM emp e INNER JOIN dept d ON e.DEPTNO = d.DEPTNO WHERE d.LOC = '上海';
34.查询emp表中，员工姓名以及姓名长度
SELECT ename,CHAR_LENGTH(ename) FROM emp;
35.查询emp表中，员工的姓名以及姓名的最后一个字符
SELECT ename,RIGHT(ename,1) FROM emp;
36.查询dept表中，查询[部门名称]和[地址]的字符拼接内容
SELECT CONCAT(dname,loc) FROM dept;
37.查询emp表中，入职时间是去年的员工信息
SELECT * from emp WHERE HIREDATE BETWEEN '2022-01-01' AND '2022-12-31';
38.查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
SELECT e.EMPNO,e.ENAME,e.JOB,d.DNAME,d.LOC FROM emp e LEFT JOIN dept d on e.DEPTNO = d.DEPTNO;
39.查询所在地区在“上海”的员工有哪些，并根据工资升序排列
SELECT e.ename,e.sal FROM emp e LEFT JOIN dept d ON e.deptno = d.deptno WHERE d.loc = '上海' ORDER BY e.sal ASC;
40.查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
SELECT e.ENAME,e.SAL,s.GRADE
FROM emp e
JOIN salgrade s
ON e.sal BETWEEN s.LOSAL AND s.upsal;

41.查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
SELECT e1.empno '员工编号', e1.ename '员工名称',e1.job '职位',e1.mgr '上级领导编号',e2.ename'上级领导姓名' FROM emp e1 LEFT JOIN emp e2 ON e1.mgr = e2.empno ;
