1.查询班级名称为WWT105的班级，在2023年2月15日未签到的学生名单
SELECT name from user,attendance where user.id=attendance.userid and userid = (SELECT id from class where name='wwt105') and date BETWEEN '2023-2-15 00:00:00' and '2023-2-15 23:59:59' and signin=1;
SELECT u.`NAME` FROM attendance a
LEFT JOIN `user` u
on a.UserId = u.id
LEFT JOIN class c
on a.ClassId = c.Id
WHERE c.`NAME` = 'WWT105'
AND a.Date = '2023-2-15'
and a.SignIn = 0; 
2.查询班级名称为WWT105的班级的男女比例
SELECT SUM(case when sex=1 then 1 else 0 END)/
       SUM(case WHEN sex=0 then 1 else 0 END)
			 FROM `user`u,class c
where u.ClassId = c.id
and c.`Name` = 'WWT105';
3.查询班级名称为WWT105的班级，在学习阶段为"功能测试阶段"，未参加考试的名单(即，分数=null)
SELECT u.`Name` FROM score s
LEFT JOIN `user` u
on s.UserId = u.Id
LEFT JOIN class c
ON c.Id = s.ClassId
WHERE c.`Name` = 'WWT105'
AND s.Score = '功能测试阶段'
AND s.Score is null;
4.查询2023年2月1日，全员都签到的班级名称
SELECT a.ClassId,COUNT(a.id),c.`Name` FROM attendance a
INNER JOIN class c
on c.id = a.ClassId
WHERE a.Date = '2023-02-01'
AND a.SignIn = 1
GROUP BY a.ClassId
HAVING COUNT(*) = (SELECT Number FROM class WHERE id = a.ClassId);
5.查询全校的男生平均年龄、女生平均年龄、全校平均年龄
SELECT '男平均' as '分组',AVG(u.Age) as '年龄' FROM `user` u where u.sex =1
UNION
SELECT '女平均',AVG(u.Age) FROM `user` u WHERE u.Sex = 0
UNION
SELECT '校平均',AVG(U.Age) FROM `user`u;
6.查询班级名称为WWT105的班级，成绩ABCDE每个等级的人数分别有多少
SELECT COUNT(u.Id),u.Grade FROM `user` u
LEFT JOIN class c
ON u.ClassId = C.id
WHERE c.`Name` = 'WWT105'
GROUP BY u.Grade
7.查询班级名称WWT105班级里，名为"阿龙”的同学参加考试的所有成绩单平均分
SELECT avg(s.score) FROM `user` U
INNER JOIN class C
ON U.ClassId = C.Id
INNER JOIN score s
on s.UserId = u.Id
WHERE C.`Name` = 'WWT105'
and U.`Name` = '阿龙';
