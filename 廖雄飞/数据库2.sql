CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '新闻id',
  `newstitle` varchar(100) NOT NULL COMMENT '新闻标题',
  `content` varchar(500) DEFAULT NULL COMMENT '新闻内容',
  `type` int(11) NOT NULL COMMENT '新闻类型',
  `status` int(11) NOT NULL COMMENT '新闻状态',
  `comment_num` int(11) DEFAULT NULL COMMENT '评论数量',
  `create_by` varchar(100) NOT NULL COMMENT '创建人',
  `createdate` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(100) NOT NULL COMMENT '更新人',
  `updatedate` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (1, 'a', 'a', 1, 1, 0, 'a', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
-- 
-- 
-- UPDATE news set newstitle='世界十大未解之谜' where status=1;
-- 
-- DELETE from news where newstitle like '世界%';
-- 
-- DELETE from news;
-- 
-- select * from news where content like '%经典%' and newstitle not like '%世界%'; 
-- 
-- select * from news where status = 1 ORDER BY comment_num desc LIMIT 10;
-- 
-- select * from news where newstitle like '________' and comment_num > 0 and `status` = 1;
-- 
-- SELECT * from news WHERE createdate < '2023-2-1 00-00-00' and createdate > '2022-12-31 23-59-59';
-- select * from news where createdate between '2023-2-1 00-00-00' and '2022-12-31 23-59-59';
-- 
-- select * from news where content is null and status=0;
-- 
-- select * from news where create_by=update_by;
-- 
-- select count(*) from news GROUP BY status ;
-- 
-- select GROUP_CONCAT(type) 
-- from news 
-- WHERE createdate>'2022-6-31' 
-- GROUP BY type 
-- HAVING COUNT(*)>10;
-- 
-- select type,avg(comment_num) from news group by type;
-- 
-- 