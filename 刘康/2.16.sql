-- CREATE TABLE `news` (
--   `id` int(11) NOT NULL auto_increment COMMENT '新闻表id',
--   `newstitle` varchar(100) NOT NULL COMMENT '新闻标题',
--   `content` varchar(500) DEFAULT NULL COMMENT '新闻内容',
--   `type` int(11) NOT NULL COMMENT '新闻类型',
--   `status` int(11) NOT NULL COMMENT '新闻状态',
--   `comment_num` int(11) DEFAULT NULL COMMENT '评论数量',
--   `create_by` varchar(100) NOT NULL COMMENT '评论数量',
--   `createdate` datetime NOT NULL COMMENT '创建日期',
--   `update_by` varchar(100) NOT NULL COMMENT '更新人',
--   `updatedate` datetime NOT NULL COMMENT '更新事件',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (1, 'a', 'a', 1, 1, 0, 'a', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (2, 'b', 'b', 2, 2, 0, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (3, 'b', '经典', 2, 2, 0, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (4, '世界简史', '经典', 2, 0, 2, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (5, 'cccccccc', '经典', 2, 0, 1, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (6, 'd', '经典', 2, 0, 32, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (7, 'e', '经典', 1, 0, 22, 'b', '2022-06-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (8, 'f', '经典', 2, 0, 12, 'b', '2022-02-01 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (9, 'g', '经典', 2, 0, 2, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');
INSERT INTO `test01`.`news` (`id`, `newstitle`, `content`, `type`, `status`, `comment_num`, `create_by`, `createdate`, `update_by`, `updatedate`) VALUES (10, 'h', '经典', 2, 0, 2, 'b', '2023-02-08 16:20:28', 'a', '2023-02-15 16:20:40');

UPDATE news set newstitle='世界十大未解之谜' where status=1;

DELETE from news where newstitle like '世界%';

DELETE from news;

select * from news where content like '%经典%' and newstitle not like '%世界%'; 

select * from news where status = 1 ORDER BY comment_num desc LIMIT 10;

select * from news where newstitle like '________' and comment_num > 0 and `status` = 1;

SELECT * from news WHERE createdate < '2023-2-1 00-00-00' and createdate > '2022-12-31 23-59-59';
select * from news where createdate between '2023-2-1 00-00-00' and '2022-12-31 23-59-59';

select * from news where content is null and status=0;

select * from news where create_by=update_by;

select count(*) from news GROUP BY status ;

select GROUP_CONCAT(type) 
from news 
WHERE createdate>'2022-6-31' 
GROUP BY type 
HAVING COUNT(*)>10;

select type,avg(comment_num) from news group by type;


