 -- 1.查询班级名称为WWT105的班级，在2023年2月1日未签到的学生名单【考勤、用户、班级】
select u.`Name` FROM attendance a
LEFT JOIN class c
on a.ClassId = c.Id
LEFT JOIN `user` u
on a.UserId = u.Id
WHERE a.Date = '2023-02-01'
and a.SignIn = 0
and c.`Name` = 'WWT105';

-- 2.查询班级名称为WWT105的班级的男女比例
SELECT sum(case when sex=1 then 1 else 0 end)/sum(case when sex=0 then 1 else 0 end) 
FROM `user` u, class c
WHERE u.ClassId = c.Id
and c.`Name` = 'WWT105';

-- 3.查询班级名称为WWT105的班级，在学习阶段为“功能测试阶段”，未参加考试的名单（即，分数=null）
select u.`Name` FROM score s
LEFT JOIN `user` u
on s.UserId = u.Id
LEFT JOIN class c
ON c.Id = s.ClassId
WHERE c.`Name` = 'WWT105'
AND s.Stage = '功能测试阶段'
and s.Score is NULL;

-- 4.查询2023年2月1日，全员都签到的班级名称
SELECT a.ClassId, COUNT(a.Id), c.`Name` FROM attendance a
INNER JOIN class c
on c.Id = a.ClassId
WHERE a.Date = '2023-02-01'
and a.SignIn = 1
GROUP BY a.ClassId
HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId);

-- 5.查询全校的男生平均年龄、女生平均年龄、全校平均年龄
SELECT '男平均' as '分组', avg(u.Age) as '年龄' FROM `user` u where u.Sex = 1
UNION
SELECT '女平均', avg(u.Age) FROM `user` u where u.Sex = 0
UNION
SELECT '校平均', avg(u.Age) FROM `user` u;

-- 6.查询班级名称为WWT105的班级，成绩ABCDE每个等级的人数分别有多少
SELECT COUNT(U.Id), U.Grade FROM `user` U
LEFT JOIN class c
ON U.ClassId = C.Id
WHERE c.`Name` = 'WWT105'
GROUP BY U.Grade

-- 7.查询班级名称WWT105班级里，名为“喵小米”的同学参加考试的所有成绩单平均分
SELECT avg(s.score) FROM `user` U
INNER JOIN class C
ON U.ClassId = C.Id
INNER JOIN score s
on s.UserId = u.Id
WHERE C.`Name` = 'WWT105'
and U.`Name` = '喵小米';


-- 录入成绩后，更新数据
DROP PROCEDURE if exists add_score; -- 删除存储函数

delimiter $$
CREATE PROCEDURE add_score(in className VARCHAR(50), in userName VARCHAR(50), in StageName VARCHAR(50), in addScore int)
BEGIN
DECLARE cid int;	-- 班级编号
DECLARE uid int;	-- 用户编号
set cid = (SELECT Id FROM class C WHERE C.`Name` = className);
set uid = (SELECT u.Id FROM class c, `user` u where c.Id = u.ClassId and C.`Name` = className AND u.`Name` = userName);
INSERT into score VALUES(DEFAULT, cid, uid, StageName, addScore, NULL);

UPDATE `user` SET GRADE = 
(SELECT case
	when Score<=100 and Score>=90 then 'A'
	when Score<=89 and Score>=75 then 'B'
	when Score<=74 and Score>=60 then 'C'
	when Score<=59 and Score>=45 then 'D'
	when Score<=44 and Score>=0 then 'E'
	ELSE
		'其他'
END '成绩等级' FROM score WHERE UserId = uid ORDER BY id desc limit 1 )
 WHERE id = uid;

UPDATE class set GRADE = 
(SELECT case
	when avg(Score) >=90 then 'A'
	when avg(Score) >=75 then 'B'
	when avg(Score) >=60 then 'C'
	when avg(Score) >=45 then 'D'
	when avg(Score) >=0 then 'E'
	ELSE '其他'
	END'平均分' FROM score WHERE ClassId = cid)
WHERE id = cid;

END $$
delimiter;


CALL add_score('WWT105', '喵小米', '接口测试阶段', 99);
