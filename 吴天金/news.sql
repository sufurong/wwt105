create table news (
id int(11) not null primary key auto_increment comment'新闻表id',
newstitle varchar(100) not null comment'新闻标题',
content varchar(500) null comment'新闻内容',
type int(20) not null comment'新闻类型，1社会、2娱乐、教育',
status int(11) not null comment'新闻状态，默认1有效，0无效',
comment_num int(10)  null comment'评论数量，默认0',
create_by varchar(100) not null comment'创建人',
createdate datetime not null comment'创建时间',
update_by varchar(100) not null comment'更新人',
updatedate datetime not null comment'更新时间'
);
insert news values(1,'世界爆炸','世界末日',3,0,1,'世界','2023-2-16','中国','2023-2-16');
insert news values(2,'十三点','乏味的',5,1,1,'奶茶饿哦怕','2023-2-16','中国','2023-2-16');
insert news values(3,'大王法务','史蒂芬',3,1,1,'奥法维持','2023-2-16','中国','2023-2-16');
insert news values(4,'爱思乏味','负电荷的人',3,0,1,'不太符合','2023-2-16','中国','2023-2-16');
insert news values(5,'挺好的发表','如果v心爱的',3,1,1,'世界','2023-2-16','中国','2023-2-16');
insert news values(6,'爱不完豆瓣','甲方公司',3,1,1,'世界','2023-2-16','中国','2023-2-16');
update news set newstitle='世界十大未解之谜' where status=1;
delete from news where newstitle like '%世界%';
select * from news where content like'经典'and newstitle not like'世界';
select * from news where status=1 order by comment_num desc limit 0,10;
select * from news where newstitle like'________' and comment_num>=1 having status=1;
select * from news where createdate between '2023-2'and '2023-2-30' having status>1;
select * from news where content is null;
select * from news where create_by = update_by;
select status,sum(status) from news group by status ;
select type from news where createdate between '2022-1-1' and '2022-6-31' group by type having count(*)>10;
select type,avg(comment_num)from news group by type;