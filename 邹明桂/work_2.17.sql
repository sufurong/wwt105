/*
 Navicat MySQL Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : localhost:3306
 Source Schema         : work_2.17

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 20/02/2023 14:05:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for attendance
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '考勤数据编号',
  `UserId` int(11) NOT NULL COMMENT '学生编号',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `ClassHour` int(11) NOT NULL COMMENT '课时',
  `Date` datetime NOT NULL COMMENT '考勤日期',
  `SignIn` int(11) NULL DEFAULT NULL COMMENT '是否签到，0未签到 1签到',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of attendance
-- ----------------------------
INSERT INTO `attendance` VALUES (1, 1, 1, 2, '2023-02-01 16:19:41', 1);
INSERT INTO `attendance` VALUES (2, 2, 1, 1, '2023-02-02 16:21:15', 0);
INSERT INTO `attendance` VALUES (3, 3, 2, 1, '2023-02-01 00:00:00', 1);
INSERT INTO `attendance` VALUES (4, 4, 1, 2, '2023-02-02 16:41:59', 0);
INSERT INTO `attendance` VALUES (5, 5, 1, 1, '2023-02-15 00:00:00', 0);
INSERT INTO `attendance` VALUES (6, 6, 1, 3, '2023-02-01 00:00:00', 1);
INSERT INTO `attendance` VALUES (7, 7, 2, 2, '2023-02-17 00:00:00', 1);

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级编号',
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级名称',
  `Number` int(11) NOT NULL COMMENT '班级人数',
  `ClassHourCount` int(11) NOT NULL COMMENT '总课时',
  `CreateDate` datetime NOT NULL COMMENT '开班时间',
  `Grade` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后平均分等级：A/B/C/D/E',
  `IsEnd` int(11) NOT NULL COMMENT '是否结课：0未结课 1已结课',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES (1, 'WWT105', 2, 5, '2023-02-01 16:12:38', 'A', 1);
INSERT INTO `class` VALUES (2, 'WWT106', 1, 4, '2023-02-02 16:13:33', 'B', 0);

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '成绩单编号',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `UserId` int(11) NOT NULL COMMENT '学生编号',
  `Stage` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学习阶段',
  `Score` int(11) NULL DEFAULT NULL COMMENT '分数',
  `Remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES (1, 1, 1, '功能测试阶段', 100, '6');
INSERT INTO `score` VALUES (2, 1, 2, '功能测试阶段', 88, '7');
INSERT INTO `score` VALUES (3, 2, 3, '功能测试阶段', 66, '8');
INSERT INTO `score` VALUES (4, 1, 4, '功能测试阶段', NULL, '9');
INSERT INTO `score` VALUES (5, 1, 1, '测开部', 98, '9');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学生编号',
  `Name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生名称',
  `ClassId` int(11) NOT NULL COMMENT '班级编号',
  `Sex` int(11) NULL DEFAULT NULL COMMENT '性别，0女 1男',
  `Age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `Grade` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后考试等级：A/B/C/D/E',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '柱子', 1, 0, 21, 'A');
INSERT INTO `user` VALUES (2, '娜娜', 1, 0, 20, 'A');
INSERT INTO `user` VALUES (3, '康子', 2, 1, 20, 'B');
INSERT INTO `user` VALUES (4, '林贤', 1, 0, 23, 'C');
INSERT INTO `user` VALUES (5, '琪琪', 1, 0, 22, 'B');
INSERT INTO `user` VALUES (6, '朱哥', 1, 1, 25, 'C');
INSERT INTO `user` VALUES (7, '刘哥', 2, 1, 26, 'D');

SET FOREIGN_KEY_CHECKS = 1;
