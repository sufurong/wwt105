 SELECT userid FROM attendance WHERE Signin = 0 and date BETWEEN '2023-02-15 00:00:00' and '2023-02-15 23:59:59'and classid in (SELECT id FROM class WHERE name='WWT105' );

select u.Name FROM attendance a LEFT JOIN class c on a.ClassId = c.Id LEFT JOIN user u on a.UserId = u.Id WHERE a.Date BETWEEN '2023-02-15 00:00:00' and '2023-02-15 23:59:59' and a.SignIn = 0 and c.Name = 'WWT105';



SELECT 
   sum(case WHEN sex=1 THEN 1 else 0 end)/
   sum(case WHEN sex=0 THEN 1 else 0 end)
 FROM user WHERE classid in (SELECT id FROM class where name='WWT105');
 
 SELECT userid FROM score WHERE stage='功能测试' and score is null and classid in (SELECT id FROM class WHERE name='WWT105' ); 
 

 SELECT name FROM class c where id in (SELECT classid FROM attendance as  a where signin = '1' ) GROUP BY a.ClassId HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId) ;

SELECT a.ClassId, COUNT(a.Id), c.Name FROM attendance a
INNER JOIN class c on c.Id = a.ClassId
WHERE a.Date BETWEEN '2023-02-15 00:00:00' and '2023-02-15 23:59:59' and a.SignIn = 1 GROUP BY a.ClassId
HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId);

 SELECT AVG(age) FROM user GROUP BY sex;
 
SELECT '男平均' as '分组', avg(u.Age) as '年龄' FROM user u where u.Sex = 1
UNION
SELECT '女平均', avg(u.Age) FROM `user` u where u.Sex = 0
UNION
SELECT '校平均', avg(u.Age) FROM `user` u;

 
 SELECT grade,COUNT(grade) FROM user where classid in (SELECT id FROM class where name='WWT105') GROUP BY grade;
 
 
 SELECT COUNT(U.Id), U.Grade FROM `user` U
LEFT JOIN class c
ON U.ClassId = C.Id
WHERE c.`Name` = 'WWT105'
GROUP BY U.Grade


SELECT AVG(score) FROM score WHERE userid in (SELECT id FROM user where name = '阿龙' and classid in (SELECT id FROM class WHERE name='WWT105'));



DROP PROCEDURE if exists add_score; -- 删除存储函数

delimiter $$
CREATE PROCEDURE add_score(in className VARCHAR(50), in userName VARCHAR(50), in StageName VARCHAR(50), in addScore int)
BEGIN
DECLARE cid int;	-- 班级编号
DECLARE uid int;	-- 用户编号
set cid = (SELECT Id FROM class C WHERE C.`Name` = className);
set uid = (SELECT u.Id FROM class c, `user` u where c.Id = u.ClassId and C.`Name` = className AND u.`Name` = userName);
INSERT into score VALUES(DEFAULT, cid, uid, StageName, addScore, NULL);

UPDATE `user` SET GRADE = 
(SELECT case
	when Score<=100 and Score>=90 then 'A'
	when Score<=89 and Score>=75 then 'B'
	when Score<=74 and Score>=60 then 'C'
	when Score<=59 and Score>=45 then 'D'
	when Score<=44 and Score>=0 then 'E'
	ELSE
		'其他'
END '成绩等级' FROM score WHERE UserId = uid ORDER BY id desc limit 1 )
 WHERE id = uid;

UPDATE class set GRADE = 
(SELECT case
	when avg(Score) >=90 then 'A'
	when avg(Score) >=75 then 'B'
	when avg(Score) >=60 then 'C'
	when avg(Score) >=45 then 'D'
	when avg(Score) >=0 then 'E'
	ELSE '其他'
	END'平均分' FROM score WHERE ClassId = cid)
WHERE id = cid;

END $$
delimiter;


CALL add_score('WWT105', '喵小米', '接口测试阶段', 99);