SELECT * FROM emp;
SELECT ENAME,sal,COMM FROM emp;
SELECT EMPNO as '员工编号',ENAME as '员工姓名',HIREDATE '入职时间' FROM emp;
SELECT * FROM emp LIMIT 2,2; 
SELECT * FROM emp ORDER BY HIREDATE DESC;
SELECT * FROM emp ORDER BY sal asc LIMIT 0,10; 
SELECT EMPNO,ENAME,SAL,HIREDATE FROM emp WHERE SAL > 6000;
SELECT * FROM emp WHERE JOB = '后端开发';
SELECT * FROM emp WHERE MGR != 2 or MGR is null;
SELECT empno,ENAME,sal FROM emp WHERE SAL BETWEEN 7000 and 10000;
SELECT * FROM emp WHERE job LIKE '%测试%' and sal <= 8000 ORDER BY EMPNO desc;
SELECT * FROM emp WHERE SAL > 6000 and comm is not null;
SELECT * FROM emp WHERE job = '测试经理' or job = '产品经理';
SELECT * FROM emp where EMPNO in (2,4,5);
SELECT * FROM emp where job != '前端开发';
SELECT * FROM emp where comm is null;
SELECT * FROM emp where comm !=0;
SELECT * FROM emp where ENAME like '%黑';
SELECT * FROM emp where ENAME like '%红%';
SELECT * FROM emp where ENAME like '__';
SELECT COUNT(*) FROM emp ;
SELECT AVG(sal) FROM emp;
SELECT ename FROM emp ORDER BY HIREDATE asc LIMIT 1;
SELECT SUM(comm) FROM emp;
SELECT COUNT(*) FROM emp GROUP BY DEPTNO;
SELECT COUNT(*),AVG(sal) FROM emp GROUP BY DEPTNO;
SELECT * FROM emp WHERE sal > 8000  GROUP BY DEPTNO HAVING MAX(sal);
SELECT DEPTNO FROM emp GROUP BY DEPTNO having COUNT(*) > 2 ORDER BY COUNT(*) desc;
SELECT * FROM emp where sal > (SELECT avg(sal) FROM emp);
SELECT * FROM emp where sal > (SELECT sal from emp where DEPTNO = 1 ORDER BY sal desc LIMIT 1 );
SELECT * FROM emp where DEPTNO  in  (SELECT DEPTNO FROM emp WHERE ename  in ('小黑') );
SELECT * FROM emp where DEPTNO  not in  (SELECT DEPTNO FROM emp WHERE ename  in ('小黑') );
SELECT * FROM emp where DEPTNO in (SELECT DEPTNO FROM dept where loc in ('上海'));
SELECT CHAR_LENGTH(ename)  FROM emp ;
SELECT SUBSTR(ENAME,-1,1)FROM emp ;
SELECT concat(dname,loc) FROM dept;
SELECT * FROM emp WHERE year(HIREDATE) =YEAR(NOW())-1;
SELECT * FROM emp e INNER JOIN dept d on e.DEPTNO=d.DEPTNO;
SELECT * FROM emp e INNER JOIN dept d on e.DEPTNO=d.DEPTNO and d.LOC = '上海' ORDER BY sal asc;

SELECT case
	when sal<=1999 and sal>=0 then '1'
	when sal<=4999 and sal>=2000 then '2'
	when sal<=9999 and sal>=5000 then '3'
	when sal<=20000 and sal>=10000 then '4'
	ELSE
		'其他'
END '工资等级',ENAME '姓名',sal '工资' FROM emp; 

SELECT * FROM emp as a left JOIN emp as e on a.mgr = e.empno;



