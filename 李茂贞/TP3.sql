-- 1-- 1.查询班级名称为WWT105的班级，在2023年2月1日未签到的学生名单【考勤、用户、班级】 
select name from user 
where  (id  in (select userid from attendance where (date between '2023-02-15 00:00:00' and '2023-02-15 23:59:59') and (signin=0)))and ( classid  in  (select id from class where name='WWT105'));

SELECT u.name 姓名 FROM attendance AS a
LEFT JOIN class AS c 
ON a.classid = c.id
LEFT JOIN user AS u 
ON a.userid = u.id
WHERE a.signin = '0' and c.name = 'WWT105' and a.date between '2023-02-15 00:00:00' and '2023-02-15 23:59:59';

select u.name  姓名 from user as u
left join class as c
on u.classid=c.id
left join attendance as a
on u.id=a.userid
WHERE a.signin = '0' and c.name = 'WWT105' and a.date between '2023-02-15 00:00:00' and '2023-02-15 23:59:59';

-- 2
-- 2.查询班级名称为WWT105的班级的男女比例
select ((select count(*) from user where sex=1 and classid=1)/(select count(*) from user where sex=0 and classid=1)) from class where name='WWT105' ;

--3

-- 3.查询班级名称为WWT105的班级，在学习阶段为“功能测试阶段”，未参加考试的名单（即，分数=null）
select name from user where classid = (select id from class where name='WWT105') and grade is null and id in (select userid from score where stage = '功能测试阶段');

select u.name from user u 
left join class c
on u.classid=c.id 
left join score s
on u.id=s.userid
where c.name='WWT105'
and s.stage = '功能测试阶段'
and s.score is null;


-- 4
select name from class where id in (select classid from attendance  a group by classid having count(*) = (select number from class where id = classid)) and signin=1 
date between '2023-02-01 00:00:00' and '2023-02-01 23:59:59'  ;

SELECT a.ClassId, COUNT(a.Id), c.`Name` FROM attendance a
INNER JOIN class c
on c.Id = a.ClassId
WHERE a.Date = '2023-02-01'
and a.SignIn = 1
GROUP BY a.ClassId
HAVING COUNT(*) = (select Number FROM class WHERE Id = a.ClassId);

-- 5
SELECT '男平均' as '分组', avg(u.Age) as '年龄' FROM `user` u where u.Sex = 1
UNION
SELECT '女平均', avg(u.Age) FROM `user` u where u.Sex = 0
UNION
SELECT '校平均', avg(u.Age) FROM `user` u;

-- 6
SELECT COUNT(U.Id), U.Grade FROM `user` U
LEFT JOIN class c
ON U.ClassId = C.Id
WHERE c.`Name` = 'WWT105'
GROUP BY U.Grade

-- 7
SELECT avg(s.score) FROM `user` U
INNER JOIN class C
ON U.ClassId = C.Id
INNER JOIN score s
on s.UserId = u.Id
WHERE C.`Name` = 'WWT105'
and U.`Name` = '喵小米';


-- 8
DROP PROCEDURE if exists add_score; 

delimiter $$
CREATE PROCEDURE add_score(in className VARCHAR(50), in userName VARCHAR(50), in StageName VARCHAR(50), in addScore int)
BEGIN
DECLARE cid int;	
DECLARE uid int;	
set cid = (SELECT Id FROM class C WHERE C.`Name` = className);
set uid = (SELECT u.Id FROM class c, `user` u where c.Id = u.ClassId and C.`Name` = className AND u.`Name` = userName);
INSERT into score VALUES(DEFAULT, cid, uid, StageName, addScore, NULL);

UPDATE `user` SET GRADE = 
(SELECT case
	when Score<=100 and Score>=90 then 'A'
	when Score<=89 and Score>=75 then 'B'
	when Score<=74 and Score>=60 then 'C'
	when Score<=59 and Score>=45 then 'D'
	when Score<=44 and Score>=0 then 'E'
	ELSE
		'其他'
END '成绩等级' FROM score WHERE UserId = uid ORDER BY id desc limit 1 )
 WHERE id = uid;

UPDATE class set GRADE = 
(SELECT case
	when avg(Score) >=90 then 'A'
	when avg(Score) >=75 then 'B'
	when avg(Score) >=60 then 'C'
	when avg(Score) >=45 then 'D'
	when avg(Score) >=0 then 'E'
	ELSE '其他'
	END'平均分' FROM score WHERE ClassId = cid)
WHERE id = cid;

END $$
delimiter;


CALL add_score('WWT105', '喵小米', '接口测试阶段', 99);
