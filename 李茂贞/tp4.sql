-- 1 查询emp表中全部员工信息
select * from emp;

-- 2 查询emp表中的[员工姓名]、[工资]、[奖金]
select ename,sla,comm from emp;
--  查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
select empno 李茂贞,ename 毛真理,hiredate 真礼貌 from emp 
-- 查询emp表中第3条开始，查询2条数据 
select * from emp limit 2,2;
-- 查询emp表中的全部数据，根据[入职时间]降序排序
select * from emp order by hiredate desc;
-- 查询emp表中工资最低的10个员工数据
select * from emp order by sal desc limit 10;
-- 查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
select empno,ename,sal,hiredate from emp where sal>6000;
-- 查询emp表中，职位为'后端开发'的员工信息
select * from emp where job = "后端开发";
-- 查询emp表中，直系上级领导编号不是“2”的员工信息
select * from emp  where mgr !=2;
-- 查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
select empno,ename,sal from emp where sal between 7000 and 10000;
-- 查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
select empno from emp where sal<=8000 and job  like '%测试%' order by empno desc;
-- 查询emp表中，工资大于6000，并且奖金不为空的员工信息
select * from emp where sal>6000 and comm is not null;
-- 查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
select * from emp where job="测试经理" or job ="产品经理";
-- 查询emp表中，员工编号是2、4、5的员工信息
select * from emp where deptno in (2,4,5);
-- 查询emp表中，职位不是'前端开发'的员工信息
select * from emp where job !="前端开发";
-- 查询emp表中奖金为null的员工信息
select * from emp where comm is null;
-- 查询emp表中有奖金的员工信息，且奖金不为0
select * from emp where comm != 0;
-- 查询emp表中，员工姓名是‘黑’结尾的员工信息
select * from emp where ename like '%黑';
-- 查询emp表中，员工姓名包含‘红’字的员工信息
select * from emp where ename like '%红%';
-- 查询emp表中，员工姓名是两个字的员工信息
select * from emp where ename like '__';
-- 在emp表中，统计员工总人数
select count(*) from emp;
-- 在emp表中，统计员工平均工资
select avg(sal) from emp;
-- 在emp表中，统计入职最早的[员工姓名]
select ename from emp order by hiredate asc limit 1;
-- 在emp表中，统计所有员工奖金总和
select sum(comm) from emp;
-- 在emp表中，统计每个部门对应的员工人数
select count(*) from emp group by deptno;
-- 在emp表中，统计每个部门的员工人数，以及该部门的平均工资
select deptno,avg(sal),count(*) from emp
group by deptno;
-- 在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
select ename,sal,deptno,max(sal) from emp 
where sal >8000 
group by deptno;
-- 在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
select deptno,count(deptno) from emp 
group by deptno
order by count(deptno) desc;
-- 查询emp表中，工资大于平均工资的员工信息
select * from emp where sal > (select avg(sal) from emp);
-- 查询emp表中，工资大于1号部门最高工资的员工信息
select * from emp where sal > (select max(sal) from emp where deptno='1' )
group by deptno;
-- 查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
select * from emp where deptno = (select deptno from emp where ename='小黑');
-- 查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
select * from emp where deptno !=(select deptno from emp where ename='小黑');
-- 查询部门所在地区是'上海'的员工信息
select * from emp as e
left join dept as d 
on e.deptno=d.deptno
where loc='上海';
-- 查询emp表中，员工姓名以及姓名长度
select ename,LENGTH(ename) from emp;
-- 查询emp表中，员工的姓名以及姓名的最后一个字符
select ename,right (ename,1) from emp;
-- 查询dept表中，查询[部门名称]和[地址]的字符拼接内容
select dname,loc,concat(dname,loc)from dept;
-- 查询emp表中，入职时间是去年的员工信息
select * from emp where hiredate between '2022-01-01'and'2022-12-31';
-- 查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
select empno,ename,job from emp as e 
left join dept as d
on e.deptno= d.deptno;

-- 查询所在地区在“上海”的员工有哪些，并根据工资升序排列
select ename from emp as e 
inner join dept as d 
on e.deptno=d.deptno where d.loc='上海'
order by sal;
-- 查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
SELECT e.ename '员工姓名', e.sal '工资', s.grade '工资等级' from emp e
left JOIN salgrade s
on e.sal between s.losal and s.upsal;
-- 查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
select e1.empno, e1.ename, e1.job e2.empno, e2.ename
from emp e1,emp e2
where e1.mgr = e2.empno;
-- 