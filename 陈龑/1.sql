CREATE TABLE user_comments(
user_id int PRIMARY KEY auto_increment COMMENT'评论表id' not NULL,
user_News_Id int COMMENT'新闻id' not null,
user_comment varchar(500) COMMENT'评论' not null,
user_Create_by VARCHAR(100) COMMENT'创建人' not null,
user_Create_date date COMMENT'创建时间' not null,
user_Update_by VARCHAR(100) COMMENT'更新人' not null,
user_Update_date date COMMENT'更新时间' NOT null 
)ENGINE=INNODB DEFAULT CHARSET=utf8;