create table comments(
id int(11) not null comment'评论表id',
News_Id int(11) not null comment '新闻id',
comment varchar(500) not null comment '评论',
Create_by varchar(100) not null comment '创建人',
Create_date date not null comment '创建时间',
Update_by varchar(100) not null comment '更新人',
Update_date date not null comment '更新时间',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6;