create table news(
   id int not null primary key auto_increment COMMENT '新闻id',
	 newstitle VARCHAR(100) NOT NULL COMMENT '新闻标题' ,
	 content VARCHAR(500) DEFAULT null COMMENT '新闻内容',
	 type int not null COMMENT '新闻类型',
	 status int NOT NULL COMMENT '新闻状态',
	 comment_num int DEFAULT NULL COMMENT '评论数量',
	 create_by VARCHAR(100) not null COMMENT '创建人',
	 createdate datetime not null COMMENT '创建时间',
	 update_by varchar(100) not null COMMENT '更新人',
	 updatedate datetime NOT NULL COMMENT '更新时间'
	 )ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 1
INSERT into news (id,newstitle,content,type,status,comment_num,create_by,createdate,update_by,updatedate) VALUES (1,'世界','耽美',2,1,0,'规划','2001-01-09','苹果','2020-08-07');
INSERT into news VALUES(NULL,'世界','天涯',2,1,0,'规划','2001-08-09','橙子','2001-08-07');
INSERT into news VALUES(NULL,'世界','黑夜',2,1,1,'阿三','2001-04-09','句子','2020-08-06');
INSERT into news VALUES(NULL,'天空','经典的',3,2,11,'阿4','2001-03-09','橘子','2020-03-06');
-- 2
UPDATE news set newstitle='世界十大未解之谜' where status=1;
-- 3
-- DELETE FROM news where newstitle='世界';
DELETE FROM news where newstitle LIKE '%世界%';
-- 4
-- SELECT * from news where content='%经典%' and newstitle!='%世界%';
SELECT *from news where content like '%经典%' and not newstitle like '%世界%';
-- 5
-- SELECT * from news ORDER BY comment_num desc LIMIT 0,10;
SELECT * from news where status = 1 ORDER BY comment_num desc LIMIT 10;
-- 6
SELECT * FROM news where newstitle like'________' and comment_num >=1 and status=1;
-- SELECT * FROM news where newstitle like'________' and comment_num >=1;
 -- 7
-- SELECT * from news where createdate='2023-01-01' and status=1;
SELECT * from news where createdate BETWEEN '2023-01-01 00:00:00' and '2023-01-01 23:59:59';
SELECT * from news where createdate >='2023-01-01 00:00:00' and  createdate<= '2023-01-01 23:59:59';
 -- 8
-- SELECT * FROM news WHERE content is null;
SELECT * FROM news WHERE content is null and status =0;
-- 9
SELECT * FROM news where create_by=update_by;
-- 10
-- SELECT status from news;
SELECT count(*) from news GROUP BY status;
-- 11
SELECT type FROM news where createdate BETWEEN '2001-06-30' and '2001-12-31' GROUP BY type having count(*)>10 ;
-- SELECT GROUP_CONCAT(type) FROM news where createdate>'2001-05-31 23:59:59' GROUP BY type HAVING COUNT(*)>10;
-- 12
SELECT type, avg(comment_num) from news group by type;