1.查询emp表中全部员工信息
SELECT * from emp;
2.查询emp表中的[员工姓名]、[工资]、[奖金]
SELECT ename,sal,comm from emp;
-- 3.查询emp表中的[员工编号]、[员工姓名]、[入职时间]，并给每一列起中文别名显示
select empno '编号',ename '姓名',hiredate '时间' from emp;
-- 4.查询emp表中第3条开始，查询2条数据
SELECT * from emp LIMIT 2,2;
-- 5.查询emp表中的全部数据，根据[入职时间]降序排序
SELECT * from emp ORDER BY hiredate DESC;
-- 6.7.查询emp表中工资最低的10个员工数据
SELECT * from emp ORDER BY sal asc LIMIT 0,10;
-- 7.查询emp表中，工资大于6000的[员工编号]、[姓名]、[工资]、[入职时间]
SELECT empno,ename,sal,hiredate from emp where sal >6000;
-- 8.查询emp表中，职位为'后端开发'的员工信息
SELECT * from emp where job='后端开发';
-- 9.查询emp表中，直系上级领导编号不是“2”的员工信息
select * from emp where not  mgr!=2;
-- 10.查询emp表中，工资范围在7000~10000之间的员工编号、姓名、工资
SELECT  empno,ename,sal from emp where sal BETWEEN 7000 and 10000;
-- 11.查询emp表中，职位包含'测试'关键词，并且工资小于等于8000的员工信息，按照员工编号降序排列
SELECT * from emp where job like '%测试%' and sal <=8000 ORDER BY empno DESC;
-- 12.查询emp表中，工资大于6000，并且奖金不为空的员工信息
SELECT * from emp where sal>6000 and comm is not null;
-- 13.查询emp表中，职位是'测试经理'或者职位是'产品经理'的员工信息
SELECT * from emp where job='测试经理' or job='产品经理';
-- 14.查询emp表中，员工编号是2、4、5的员工信息
SELECT * from emp where empno in(2,4,5);
-- 15.查询emp表中，职位不是'前端开发'的员工信息
SELECT * from emp where not job='前端开发';
-- 16.查询emp表中奖金为null的员工信息
SELECT * from emp where comm is null;
-- 17.查询emp表中有奖金的员工信息，且奖金不为0
SELECT * from emp where comm!=0;
-- 18.查询emp表中，员工姓名是‘黑’结尾的员工信息
select * from emp where ename like '%黑';
-- 19.查询emp表中，员工姓名包含‘红’字的员工信息
select * from emp where ename like '%红%';
-- 20.查询emp表中，员工姓名是两个字的员工信息
select * from emp where ename like '__';
-- 21.在emp表中，统计员工总人数
SELECT sum(empno) from emp;
-- 22.在emp表中，统计员工平均工资
SELECT avg(sal) from emp;
-- 23.在emp表中，统计入职最早的[员工姓名]
SELECT ename,hiredate
from emp 
ORDER BY hiredate asc
LIMIT 1;
-- 24.在emp表中，统计所有员工奖金总和
SELECT SUM(comm) from emp;
-- 25.在emp表中，统计每个部门对应的员工人数
SELECT deptno,COUNT(ename)
from emp
GROUP BY deptno;

-- 26.在emp表中，统计每个部门的员工人数，以及该部门的平均工资
SELECT deptno,COUNT(ename),avg(sal)
from emp
GROUP BY deptno

-- 27.在emp表中，统计工资大于8000的员工，分布在哪些部门，且该部门的最高工资是多少
SELECT deptno,MAX(sal)
from emp
WHERE sal>'8000'
GROUP  BY deptno;

-- 28.在emp表中，统计人数大于2人的部门编号，并根据人数倒序排列
SELECT  deptno
from emp  
GROUP BY deptno
HAVING COUNT(deptno) >2
ORDER BY deptno DESC;
-- 29.查询emp表中，工资大于平均工资的员工信息
SELECT * from emp 
where sal >(SELECT avg(sal) from emp);
-- 30.查询emp表中，工资大于1号部门最高工资的员工信息
SELECT *
from emp 
where (ename,sal)=(SELECT MAX(deptno),MAX(sal));
-- 31.查询emp表中，与姓名叫'小黑'是同一个部门的其他员工信息
SELECT *from emp 
where deptno=(SELECT deptno FROM emp WHERE ename='小黑') and
ename !='小黑';
-- 32.查询emp表中，与姓名叫'小黑'不是同一个部门的员工信息
SELECT * FROM emp 
WHERE DEPTNO!=(SELECT DEPTNO FROM emp WHERE ENAME='小黑');

-- 33.查询部门所在地区是'上海'的员工信息
SELECT *
from emp e
INNER JOIN dept d
on e.DEPTNO=d.DEPTNO
where loc='上海';
-- 34.查询emp表中，员工姓名以及姓名长度
SELECT ename,CHAR_LENGTH(ename) from emp ;
-- 35.查询emp表中，员工的姓名以及姓名的最后一个字符
SELECT ename,substr(ename,-1,1) from emp ;
-- 36.查询dept表中，查询[部门名称]和[地址]的字符拼接内容
SELECT CONCAT(dname,loc) from dept;
-- 37.查询emp表中，入职时间是去年的员工信息
SELECT * from emp
WHERE YEAR(hiredate) ='2022';
-- 38.查询emp表中，[员工编号]、[员工姓名]、[职位]，以及dept表中对应的[部门名称]、[部门地址]
SELECT empno,ename,job,dname,loc 
from emp e
LEFT JOIN dept d
on e.deptno=d.DEPTNO;
-- 39.查询所在地区在“上海”的员工有哪些，并根据工资升序排列
SELECT ename
from emp e
INNER JOIN dept d
on e.DEPTNO=d.DEPTNO
where loc='上海'
ORDER BY sal asc;
-- 40.查询emp表中，[员工姓名]、[工资]、以及salgrade表中对应的[工资等级]，且取别名
SELECT ename'员工姓名',sal'工资',grade'工资等级' 
from emp ,salgrade
where sal BETWEEN losal and upsal;

-- SELECT e.ENAME,e.SAL,s.GRADE
-- FROM emp e
-- INNER JOIN salgrade s
-- ON e.sal BETWEEN s.LOSAL AND s.upsal
-- GROUP BY s.GRADE;

-- 41.查询emp表中，[员工编号]、[员工姓名]、[职位]、[上级领导编号]、[上级领导姓名]
SELECT  e.empno,e.ename,e.job,e.mgr,e.hiredate,e.sal,e.comm,e.deptno 
from emp e 
INNER JOIN emp p
on e.empno=p.empno;
