CREATE TABLE `students` (
  `id` int(11) NOT NULL COMMENT '编号',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `sex` int(11) DEFAULT NULL,
  `province` varchar(10) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SELECT * from students where name in ('张三','李四','王五');


SELECT province from students group by province;

select province, count(age) from students GROUP BY province; 

select province from students GROUP BY province HAVING count(*)>10;

select province, GROUP_CONCAT(id) from students GROUP BY province; 